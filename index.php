
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Abbott</title>
<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="css/styles.css">


</head>

<body>
<div class="container-fluid">
    <div class="row logo-nav">
        <div class="col-12 col-md-4">
            <img src="img/abbot_logo.png" class="img-fluid logo" style="max-height:90px;" alt=""/> 
        </div>
        <div class="col-12 col-md-4 offset-md-4 text-right">
           <!-- <img src="img/apdrops.jpg" class="img-fluid logo" alt=""/> -->
        </div>
    </div>
    <div class="row mt-2">
  
	
      <div class="col-12 col-md-6 offset-md-4 col-lg-4">
            <div class="login">
                <form id="login-form" method="post" role="form">
                  <div id="login-message"></div>
                  <div class="input-group">
                    <input type="text" class="form-control" placeholder="Topic" aria-label="Name" aria-describedby="basic-addon1" value="" name="topic" required>
                  </div>              
                  <div class="input-group">
                    <input type="text" class="form-control" placeholder="Event status" aria-label="Email" aria-describedby="basic-addon1" name="event" value=""  required>
                  </div>
                  <div class="input-group">
                    <input type="date" class="form-control" placeholder="City" aria-label="City" aria-describedby="basic-addon1" name="date" id="city" value=""  required >
                  </div>
                  <div class="input-group">
                    <input type="time" class="form-control" placeholder="" aria-label="Hospital" aria-describedby="basic-addon1" name="time" value=""  required>
                    
                   </div>
                   <div class="input-group">
                    <input type="url" class="form-control" placeholder="Drive Link" aria-label="Hospital" aria-describedby="basic-addon1" name="drive" value="" id="hospital" required>
                    
                   </div>
                   <div class="input-group">
                    <input type="url" class="form-control" placeholder="Userslist link" aria-label="userslist" aria-describedby="basic-addon1" name="userslist" value="" id="hospital" required>
                    
                   </div>
                  <div class="input-group">
                    <button id="login" class="btn btn-primary btn-md login-button" type="submit">Click to add</button>
                  </div>
                </form>
            </div>
        
        </div>
    </div>
</div>

<script src="js/jquery.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script>
$(document).on('submit', '#login-form', function()
{  
  $.post('chkforlogin.php', $(this).serialize(), function(data)
  {
     // console.log(data);
      if(data == 's')
   
      {
          $('#login-message').text('data is added please Check in admin panel');
          $('#login-message').addClass('alert-success');
          return false;
      }
      else
      {
          $('#login-message').text(data);
          $('#login-message').addClass('alert-danger');
          return false;
      }
  });
  
  return false;
});
</script>


</body>
</html>