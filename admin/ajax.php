<?php
require_once "../config.php";

if(isset($_POST['action']) && !empty($_POST['action'])) {
    
    $action = $_POST['action'];
    
    switch($action) {
        
        
        case 'getusers':
        
            if (isset($_POST["page"])) 
            { 
                $page  = $_POST["page"]; 
            }
            else { 
                $page=1; 
            }
            
            $start_from = ($page-1) * $limit;
            $today=date('Y/m/d H:i:s');
            $sql = "SELECT COUNT(id) as count FROM tbl_users ";  
            $rs_result = mysqli_query($link, $sql) or die(mysqli_error($link)); 
            $row = mysqli_fetch_assoc($rs_result);
            $loggedin = $row['count'];
        
            
            $sql = "SELECT COUNT(id) as count FROM tbl_users ";  
            $rs_result = mysqli_query($link, $sql) or die(mysqli_error($link)); 
            $row = mysqli_fetch_assoc($rs_result);
            $total_records = $row['count'];  
            $total_pages = ceil($total_records / $limit);
            ?>
            <div class="row user-info">
                <!-- <div class="col-6">
                    Total Users: <?php echo $total_records; ?>
                </div>
                <div class="col-6">
                    Currently Logged In: <div id="logged-in"><?php echo $loggedin; ?></div>
                </div> -->
            </div> 
            <div class="row user-details">
                <div class="col-12">
                    <table class="table table-striped">
                      <thead class="thead-inverse">
                        <tr>
                          <th>Topic</th>
                          <th> Event Status</th>
                          <th>Date</th>
                          <th>Time</th>
                          <th>Download</th>
                          <th>Users/Faculty Download</th>
                          <!-- <th>Last Login Time</th>
                          <th>Last Logout Time</th> -->
                        </tr>
                      </thead>
                      <tbody>
                      <?php		
                        $query="select * from tbl_users  ";
                        $res = mysqli_query($link, $query) or die(mysqli_error($link));
                        $loggedin = 0;
                      //  $url_=$data['speciality']; 
                        while($data = mysqli_fetch_assoc($res))
                        {
                         
                        ?>

                          <tr>
                            <td><?php echo $data['user_email']; ?></td>
                            <td><?php echo $data['user_name']; ?></td>
                            <td><?php echo $data['city']; ?></td>
                            <td><?php echo $data['hospital']; ?></td>
                            <td id="text"><?php echo $data['speciality']; ?>
                            <input type="button" class="text-success" onclick="copyToClipboard('#text')" value="Click here to copy"></td>
                            <!-- <td> <a href=".'<?php echo $data['speciality'];'.' ?>" target="_blank" >URl</a></td>
                           -->
                           <td id="text_users"><?php echo $data['userslist']; ?>
                            <input type="button" class="text-success" onclick="copyToClipboard_users('#text_users')" value="Click here to copy">
                          </td>
                         
                            <?php 
                        //   foreach($results as $result){
                        //     $result = $data['speciality'];;
                        //  }
                            $result = $data['speciality'];;
                          ?>
                          <td><a href="<?php echo $result ?>" target="_blank" rel="noopener noreferrer"></a></td>
                            <!-- <a href="<?php echo $data['drive']; ?>" target="_blank"><td><?php echo $data['speciality']; ?></td></a> -->
                          </tr>
                  
                      <?php			
                        }
                      ?>
                  
                    </table>  
                </div>
            </div>
            <nav>
              <ul class="pagination pagination-sm" id="pagination">
                <?php if(!empty($total_pages)):for($i=1; $i<=$total_pages; $i++):  
                            if($i == 1):?>
                     <li onClick="update(<?php echo $i;?>)" class="page-item <?php if($_POST['page'] == $i) echo 'active'; ?>" id="<?php echo $i;?>">
                      <a class="page-link" href="#" ><?php echo $i;?></a>
                    </li>
                <?php else:?>
                    <li onClick="update(<?php echo $i;?>)" class="page-item <?php if($_POST['page'] == $i) echo 'active'; ?>" id="<?php echo $i;?>">
                      <a class="page-link" href="#" ><?php echo $i;?></a>
                    </li>
                <?php endif;?>
                <?php endfor;endif;?>
              </ul>
            </nav>
            <?php
        
            
        break;
        
      
    }
    
}


?>


<script src="../js/jquery.min.js"></script>
<script src="../js/bootstrap.min.js"></script>
<script>

function copyToClipboard(element) {
  var $temp = $("<input>");
  $("body").append($temp);
  $temp.val($(element).text()).select();
  document.execCommand("copy");
  $temp.remove();
}
function copyToClipboard_users(element) {
  // alert("hello");
  var $temp = $("<input>");
  $("body").append($temp);
  $temp.val($(element).text()).select();
  document.execCommand("copy");
  $temp.remove();
}
// function copyToClipboard_users(element) {
//   var $temp = $("<input>");
//   $("body").append($temp);
//   $temp.val($(element).text()).select();
//   document.execCommand("copy");
//   $temp.remove();
// }
</script>
