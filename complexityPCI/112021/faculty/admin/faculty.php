<?php
	require_once "../config.php";
	
?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Faculty</title>
<link rel="stylesheet" type="text/css" href="../css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="../css/styles.css">

</head>

<body class="admin">
<nav class="nav navbar navbar-light">
    <a class="nav-link" href="#"><img class="navbar-brand logo" src="../img/abbot_logo.png"></a>
</nav>

<div class="container-fluid">
     <div class="row login-info links">   
        <div class="col-8 text-left">
            <a href="../../admin/questions.php">back</a>
             <!-- |  <a href="users.php">Faculty</a> -->
          
        </div>
        <!-- <div class="text-right">
            <a href="faculty.php" class="">Team details</a>
            </div> -->
    </div>
    <div class="row mt-1">
        <div class="col-12">
            <a href="export_faculty.php"><img src="excel.png" height="45" alt=""/></a>
        </div>
    </div>
    <div class="row mt-1">
        <div class="col-12">
            <div id="users"> </div>
        </div>
    </div>
</div>


<script src="../js/jquery.min.js"></script>
<script src="../js/bootstrap.min.js"></script>
<script>
$(function(){
    getUsers('1');
});

function update(pageNum)
{
  getUsers(pageNum);
}

function getUsers(pageNum)
{
    $.ajax({
        url: 'ajax.php',
        data: {action: 'faculty', page: pageNum},
        type: 'post',
        success: function(response) {
            
            $("#users").html(response);
            
        }
    });
    
}


</script>

</body>
</html>