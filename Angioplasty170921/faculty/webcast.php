<?php
	require_once "config.php";
	
	if(!isset($_SESSION["user_email"]))
	{
		header("location: index.php");
		exit;
	}
	
	if(isset($_GET['action']) && !empty($_GET['action'])) 
    {
        $action = $_GET['action'];
        if($action == "logout")
        {
            $logout_date   = date('Y/m/d H:i:s');
            $user_email=$_SESSION["user_email"];
            
            $query="UPDATE tbl_user set logout_date='$logout_date', logout_status='0' where user_email='$user_email'  and eventname='$event_name'";
            $res = mysqli_query($link, $query) or die(mysqli_error($link));

            unset($_SESSION["user_name"]);
            unset($_SESSION["user_email"]);
            
            header("location: index.php");
            exit;
        }

    }

    
	
?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Abbott</title>
<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="css/styles.css">
</head>
<style>

body{
    background-color: #cceeff;
}
/* .vertical-center {
  margin: 0;
  position: absolute;
  top: 50%;
-ms-transform: translateX(-50%);
transform: translateY(-50%);
  transform: translateX(-50%);
} */

.center {
    display: flex;
  justify-content: center;
  align-items: center;
  height: 200px;
  
}


</style>
<body>
<div class="container-fluid">
    <div class="row">
       
        <div class="col-12 col-md-4 offset-md-4 text-right">
           <!-- <img src="img/apdrops.jpg" class="img-fluid logo" alt=""/> -->
        </div>
    </div>
    <div class="row login-info ">
        <div class="col-12 p-1 text-right">
          Hello, <?php echo $_SESSION['user_name']; ?>
        </div>
    </div>
    <h2 class="center"><strong class="text-info">Please Click Below Button To Join The Meeting By Using Your Full Name</strong></h2>
<div class="center">

  <a href="https://streamyard.com/hr6yz6zk2k">
  <button class="btn-lg btn-info w-100" >JOIN</button></a> 

</div>

<div class="container">
  <div class="vertical-center text-center">

  </div>
</div>

		  <!--
          <center><br>
        <img src="img/logo2.jpg" class="img-fluid" alt=""/> 
        </center>
		-->
        </div>
    </div>
    
</div>

<script src="js/jquery.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script>
$(function(){
    //$('#videolinks').html('If you are unable to watch video, <a href="#" onClick="changeVideo()">click here</a>');
	$(document).on('submit', '#question-form form', function()
    {  
            $.post('submitques.php', $(this).serialize(), function(data)
            {
                if(data=="success")
                {
                  $('#ques-message').text('Your question is submitted successfully.');
                  $('#ques-message').removeClass('alert-danger').addClass('alert-success').fadeIn().delay(2000).fadeOut();
                  $('#question-form').find("textarea").val('');
                }
                else 
                {
                  $('#ques-message').text(data);
                  $('#ques-message').removeClass('alert-success').addClass('alert-danger').fadeIn().delay(5000).fadeOut();
                }
                
            });
        
      
      return false;
    });
});

function update()
{
    $.ajax({ url: 'ajax.php',
         data: {action: 'update'},
         type: 'post',
         success: function(output) {
         }
});
}
setInterval(function(){ update(); }, 30000);


function changeVideo()
{
    var wc = $('#webcast').attr("src");
    console.log(wc);
    if(wc == "video.php")
    {
        $('#webcast').attr("src","video_bkup.php");
    }
    else
    {
        $('#webcast').attr("src","video.php");
    }
}

</script>

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-93480057-13"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-93480057-13');
</script>

</body>
</html>