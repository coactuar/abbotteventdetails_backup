-- phpMyAdmin SQL Dump
-- version 4.6.6deb5ubuntu0.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Jan 14, 2022 at 06:28 AM
-- Server version: 5.7.36-0ubuntu0.18.04.1
-- PHP Version: 7.2.24-0ubuntu0.18.04.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `abbott`
--

-- --------------------------------------------------------

--
-- Table structure for table `pageupdater`
--

CREATE TABLE `pageupdater` (
  `id` int(11) NOT NULL,
  `picture` varchar(255) NOT NULL,
  `player_code` text NOT NULL,
  `eventname` varchar(150) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_questions`
--

CREATE TABLE `tbl_questions` (
  `id` int(11) NOT NULL,
  `user_name` varchar(255) NOT NULL,
  `user_email` varchar(255) NOT NULL,
  `user_question` varchar(500) NOT NULL,
  `asked_at` datetime NOT NULL,
  `eventname` varchar(255) NOT NULL,
  `speaker` int(11) NOT NULL DEFAULT '0',
  `answered` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_questions`
--

INSERT INTO `tbl_questions` (`id`, `user_name`, `user_email`, `user_question`, `asked_at`, `eventname`, `speaker`, `answered`) VALUES
(1, 'V', 'viraj@coact.co.in', 'hi\r\n', '2021-02-18 17:32:42', 'Abbott', 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_users`
--

CREATE TABLE `tbl_users` (
  `id` int(11) NOT NULL,
  `user_name` varchar(255) NOT NULL,
  `user_email` varchar(500) NOT NULL,
  `city` varchar(100) NOT NULL,
  `hospital` varchar(100) NOT NULL,
  `speciality` varchar(100) DEFAULT NULL,
  `reg_no` varchar(50) DEFAULT NULL,
  `joining_date` datetime NOT NULL,
  `login_date` datetime DEFAULT NULL,
  `logout_date` datetime DEFAULT NULL,
  `logout_status` int(11) NOT NULL DEFAULT '0' COMMENT '1=loggedin',
  `eventname` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_users`
--

INSERT INTO `tbl_users` (`id`, `user_name`, `user_email`, `city`, `hospital`, `speciality`, `reg_no`, `joining_date`, `login_date`, `logout_date`, `logout_status`, `eventname`) VALUES
(1, 'Test', 'test@test.com', 'Test', 'Test', NULL, NULL, '2021-02-18 10:59:51', '2021-02-18 20:54:59', '2021-02-18 20:56:00', 0, 'Abbott'),
(2, 'V', 'viraj@coact.co.in', 'Pune', 'NH', NULL, NULL, '2021-02-18 10:59:55', '2021-02-18 17:16:33', '2021-02-18 17:33:04', 0, 'Abbott'),
(3, 'Mandakini', 'mandakini.bhatia@abbott.com', 'Mum', 'Abbott', NULL, NULL, '2021-02-18 11:03:21', '2021-02-18 11:03:21', '2021-02-18 11:03:51', 0, 'Abbott'),
(4, 'reynold', 'reynold@coact.co.in', 'Bangalore', 'Coact', NULL, NULL, '2021-02-18 11:19:55', '2021-03-09 16:42:53', '2021-03-09 16:44:29', 0, 'Abbott'),
(5, 'Nishanth', 'nishanth@coact.co.in', 'Bangalore', 'COACT', NULL, NULL, '2021-02-18 11:20:30', '2021-02-18 11:20:30', '2021-02-18 11:30:42', 0, 'Abbott'),
(6, 'rakshith shetty', 'rakshith@neosoulsoft.com', 'banglore', 'kmc', NULL, NULL, '2021-02-18 11:55:56', '2021-02-18 11:55:56', '2021-02-18 21:32:30', 0, 'Abbott'),
(7, 'MW APP ', 'mw.app81@gmail.com', 'Mumbai ', 'Somaiya ', NULL, NULL, '2021-02-18 12:44:55', '2021-02-20 06:36:18', '2021-02-20 06:36:48', 0, 'Abbott'),
(8, 'Zoheb Shaikh', 'zoheb.shaikh@abbott.com', 'Mumbai', 'Abbott', NULL, NULL, '2021-02-18 14:13:24', '2021-02-18 19:18:26', '2021-02-18 19:26:32', 0, 'Abbott'),
(9, 'neeraj', 'neeraj@coact.co.in', 'nellore', 'cvdvcdz ', NULL, NULL, '2021-02-18 16:13:14', '2021-04-07 14:27:51', '2021-04-07 14:30:21', 0, 'Abbott'),
(10, 'Dr neeta athanikar', 'drnnathanikar@gmail.com', 'Mumbai', 'Criticare', NULL, NULL, '2021-02-18 17:42:50', '2021-02-18 20:41:58', '2021-02-18 20:47:58', 0, 'Abbott'),
(11, 'Akshat Jharia', 'akshatjharia@gmail.com', 'Bangalore', 'COACT', NULL, NULL, '2021-02-18 18:29:10', '2021-02-18 18:42:39', '2021-02-18 18:43:09', 0, 'Abbott'),
(12, 'Amal', 'amal.vyas@abbott.com', 'Mumbai ', 'Abbott', NULL, NULL, '2021-02-18 18:55:33', '2021-02-18 18:55:33', '2021-02-18 20:44:53', 0, 'Abbott'),
(13, 'Aj', 'aj@gmail.com', 'Bng', 'Coact', NULL, NULL, '2021-02-18 18:57:59', '2021-02-18 18:57:59', '2021-02-18 19:05:48', 0, 'Abbott'),
(14, 'Deepal', 'deepak.mondal@abbott.com', 'Mumbai', 'Abbott ', NULL, NULL, '2021-02-18 18:58:17', '2021-02-18 19:46:38', '2021-02-18 20:47:49', 0, 'Abbott'),
(15, 'Sujatha ', 'sujatha@coact.co.in', 'Bangalore ', 'Bowling ', NULL, NULL, '2021-02-18 18:58:18', '2021-02-18 19:07:46', '2021-02-18 19:09:16', 0, 'Abbott'),
(16, 'ramkrishna.sundaram@abbott.com', 'ramkrishna.sundaram@abbott.com', 'Pune', 'Abbott Healthcare Pvt Ltd', NULL, NULL, '2021-02-18 18:58:42', '2021-02-18 18:58:42', '2021-02-18 20:56:12', 0, 'Abbott'),
(17, 'Dr Jeswani', 'drkanhaiyajeswani@yahoo.co.in', 'Pune', 'Inlaks', NULL, NULL, '2021-02-18 19:00:09', '2021-02-18 19:00:09', '2021-02-18 19:04:12', 0, 'Abbott'),
(18, 'Kaushik Sheth', 'kaushik_sheth@hotmail.com', 'Pune', 'Ruby Hall Clinic', NULL, NULL, '2021-02-18 19:00:54', '2021-02-18 20:40:26', '2021-02-18 20:41:11', 0, 'Abbott'),
(19, 'Krishnakumar G', 'krishnakumar.g@abbott.com', 'Mumbai', 'NA', NULL, NULL, '2021-02-18 19:01:58', '2021-02-18 20:14:35', '2021-02-18 20:47:57', 0, 'Abbott'),
(20, 'Test', 'test@gmail.com', 'Pune ', 'Test ', NULL, NULL, '2021-02-18 19:02:12', '2021-02-18 19:02:12', '2021-02-18 19:17:54', 0, 'Abbott'),
(21, 'Subroto Bhattacharjee', 'subroto.bhattacharjee@abbott.com', 'Pune', 'NA', NULL, NULL, '2021-02-18 19:02:51', '2021-02-18 19:23:09', '2021-02-18 20:48:11', 0, 'Abbott'),
(22, 'Aniruddha Pawar', 'dr.aniruddha7910@gmail.com', 'Thane', 'KMF', NULL, NULL, '2021-02-18 19:03:11', '2021-02-18 19:03:11', '2021-02-18 20:48:56', 0, 'Abbott'),
(23, 'Dr Niraj Raghani', 'drnirajraghani@yahoo.co.in', 'Amravati', 'Zenith', NULL, NULL, '2021-02-18 19:03:44', '2021-02-18 19:03:44', '2021-02-18 19:04:45', 0, 'Abbott'),
(24, 'Dr Hore', 'hore_dr@yahoo.com', 'Nagpur', 'Wockhardt', NULL, NULL, '2021-02-18 19:04:17', '2021-02-18 19:04:17', '2021-02-18 19:06:47', 0, 'Abbott'),
(25, 'Pooja', 'test@coact.co.in', 'Mumbai ', 'Test', NULL, NULL, '2021-02-18 19:04:22', '2021-02-18 19:04:22', '2021-02-18 19:14:54', 0, 'Abbott'),
(26, 'Suraj Ingole', 'surajingoledr@gmail.com', 'Kolhapur', 'Sai Cardiac hospital', NULL, NULL, '2021-02-18 19:04:47', '2021-02-18 19:34:01', '2021-02-18 20:02:02', 0, 'Abbott'),
(27, 'Darshan Malushte', 'darshan.malushte@abbott.com', 'Mumbai', 'AV', NULL, NULL, '2021-02-18 19:06:03', '2021-02-18 20:46:01', '2021-02-18 20:53:42', 0, 'Abbott'),
(28, 'Dr Rajesh Badani', 'badani_rajesh@yahoo.co.in', 'Pune', 'Aditya Birla', NULL, NULL, '2021-02-18 19:06:18', '2021-02-18 19:06:18', '2021-02-18 19:07:18', 0, 'Abbott'),
(29, 'dr Ganjewar', 'shailendra_ganjewar@yahoo.co.in', 'nagpur', 'kingsway', NULL, NULL, '2021-02-18 19:06:56', '2021-02-18 19:06:56', '2021-02-18 21:01:16', 0, 'Abbott'),
(30, 'Ashish Lohgaonkar', 'ashish.lohgaonkar@abbott.com', 'Kolhapur', 'Abbott', NULL, NULL, '2021-02-18 19:07:06', '2021-02-18 19:07:06', '2021-02-18 20:32:36', 0, 'Abbott'),
(31, 'Vandanaa', 'vandanaanarsian@gmail.com', 'Pune', 'Ruby Hall', NULL, NULL, '2021-02-18 19:08:23', '2021-02-18 19:08:23', '2021-02-18 19:11:24', 0, 'Abbott'),
(32, 'VAIBHAV DEDHIA', 'dedhiadoc@gmail.com', 'Mumbai', 'HNH', NULL, NULL, '2021-02-18 19:11:43', '2021-02-18 19:11:43', '2021-02-18 19:53:44', 0, 'Abbott'),
(33, 'Amit sharma', 'amitthedoc@hotmail.com', 'Mumbai', 'Juhu', NULL, NULL, '2021-02-18 19:12:03', '2021-02-18 19:12:03', '2021-02-18 20:19:26', 0, 'Abbott'),
(34, 'Sagar Mhadgut ', 'sagar.mhadgut@abbott.com', 'Mumbai', 'Abbott', NULL, NULL, '2021-02-18 19:12:32', '2021-02-18 19:12:32', '2021-02-18 19:31:18', 0, 'Abbott'),
(35, 'Shantanu Deshpande', 'drshantanud@yahoo.com', 'mumbai', 'dyp', NULL, NULL, '2021-02-18 19:16:30', '2021-02-18 19:16:30', '2021-02-18 20:10:31', 0, 'Abbott'),
(36, 'Pooja', 'pooja@coact.co.in', 'Mumbai', 'test', NULL, NULL, '2021-02-18 19:17:50', '2021-02-18 19:17:50', '2021-02-18 20:53:51', 0, 'Abbott'),
(37, 'Aiswarya', 'aiswaryasankar4322@gmail.com', 'Kochi', 'Amrita', NULL, NULL, '2021-02-18 19:18:41', '2021-02-18 19:18:41', '2021-02-18 20:00:48', 0, 'Abbott'),
(38, 'Satish Chirde', 'drsrc1@rediffmail.com', 'yavatmal', 'Shri Datta Hospital and research Center', NULL, NULL, '2021-02-18 19:19:28', '2021-02-21 22:19:13', '2021-02-21 22:19:38', 0, 'Abbott'),
(39, 'Aniket Gadre', 'draniketgadre@gmail.com', 'Pune', 'Deenanath Mangeshkar ', NULL, NULL, '2021-02-18 19:21:35', '2021-02-18 19:21:35', '2021-02-18 19:41:42', 0, 'Abbott'),
(40, 'vandanaanarsian', 'vandamaamarsian@gmail.com', 'PUNE', 'ruby hall', NULL, NULL, '2021-02-18 19:25:03', '2021-02-18 19:25:03', '2021-02-18 20:48:36', 0, 'Abbott'),
(41, 'Sagar Mhadgut', 'sagar.mhagut@abbott.com', 'Mumbai', 'Abbott', NULL, NULL, '2021-02-18 19:25:36', '2021-02-18 19:25:36', '2021-02-18 20:06:06', 0, 'Abbott'),
(42, 'Sruthi S Prathap', 'sruthysprathap20@gmail.com', 'Ernakulam', 'Amrita', NULL, NULL, '2021-02-18 19:34:19', '2021-02-18 19:34:19', '2021-02-18 19:34:49', 0, 'Abbott'),
(43, 'TEST', 'udit.mathur144@jtb-india.com', 'Gurgaon', 'Test', NULL, NULL, '2021-02-18 19:35:33', '2021-03-11 17:17:59', '2021-03-11 17:18:29', 0, 'Abbott'),
(44, 'GANESH VITTHAL BENDE', 'ganeshbende7@yahoo.com', 'MUMBAI', 'ZYNOVA HOSPITALS', NULL, NULL, '2021-02-18 20:10:07', '2021-02-18 20:10:07', '2021-02-18 21:02:38', 0, 'Abbott'),
(45, 'Ameen Bubere', 'ameen.bubere@abbott.com', 'Mumbai', 'Abbott', NULL, NULL, '2021-02-18 20:31:29', '2021-02-18 20:31:29', '2021-02-18 20:37:17', 0, 'Abbott'),
(46, 'Suniil Hegade', 'suniil.hegade@abbott.com', 'Mumbai', 'Abbott', NULL, NULL, '2021-02-18 20:34:17', '2021-02-18 20:47:05', '2021-02-18 20:48:25', 0, 'Abbott'),
(47, 'Test', 'tt@ty.com', 'Pune', 'Jj', NULL, NULL, '2021-02-18 20:55:12', '2021-02-18 20:55:12', '2021-02-18 21:13:34', 0, 'Abbott'),
(48, 'Rohit Thakare', 'rohit.thakare111@gmail.com', 'Mumbai', 'Bombay hospital', NULL, NULL, '2021-02-18 21:35:01', '2021-02-18 23:36:41', '2021-02-18 23:37:11', 0, 'Abbott'),
(49, 'Mayank', 'drmayankagarwal85@gmail.com', 'Guwahati', 'Health city hospital', NULL, NULL, '2021-02-19 13:41:07', '2021-02-19 13:41:07', '2021-02-19 13:41:37', 0, 'Abbott'),
(50, 'akki', 'aj@jcom', 'Bng', 'Coact', NULL, NULL, '2021-02-20 14:14:24', '2021-02-20 14:14:24', '2021-02-20 14:15:24', 0, 'Abbott'),
(51, 'Dr.Ketan', 'meetkat312@gmail.com', 'Mumbai', 'L.T.M.G.Hospital', NULL, NULL, '2021-02-21 00:26:39', '2021-02-21 00:26:39', '2021-02-21 00:27:09', 0, 'Abbott');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `pageupdater`
--
ALTER TABLE `pageupdater`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_questions`
--
ALTER TABLE `tbl_questions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_users`
--
ALTER TABLE `tbl_users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `pageupdater`
--
ALTER TABLE `pageupdater`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_questions`
--
ALTER TABLE `tbl_questions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `tbl_users`
--
ALTER TABLE `tbl_users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=52;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
