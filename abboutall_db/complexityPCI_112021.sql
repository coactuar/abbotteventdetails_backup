-- phpMyAdmin SQL Dump
-- version 4.6.6deb5ubuntu0.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Jan 14, 2022 at 08:54 AM
-- Server version: 5.7.36-0ubuntu0.18.04.1
-- PHP Version: 7.2.24-0ubuntu0.18.04.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `complexityPCI_112021`
--

-- --------------------------------------------------------

--
-- Table structure for table `faculty`
--

CREATE TABLE `faculty` (
  `id` int(11) NOT NULL,
  `user_name` varchar(200) NOT NULL,
  `user_email` varchar(200) NOT NULL,
  `city` varchar(100) NOT NULL,
  `hospital` varchar(100) NOT NULL,
  `speciality` varchar(100) DEFAULT NULL,
  `reg_no` varchar(100) DEFAULT NULL,
  `joining_date` varchar(100) NOT NULL,
  `login_date` date DEFAULT NULL,
  `logout_date` date DEFAULT NULL,
  `logout_status` int(11) NOT NULL DEFAULT '0' COMMENT '1 = logged in',
  `eventname` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `faculty`
--

INSERT INTO `faculty` (`id`, `user_name`, `user_email`, `city`, `hospital`, `speciality`, `reg_no`, `joining_date`, `login_date`, `logout_date`, `logout_status`, `eventname`) VALUES
(1, 'Nishanth', 'nishanth@coact.co.in', 'Bengaluru', 'cuibxj', NULL, NULL, '2021/11/09 13:01:23', '2021-11-09', '2021-11-09', 1, 'Abbott'),
(2, 'Vibhav ', 'vibhav@coact.co.in', 'Pune', 'ABC', NULL, NULL, '2021/11/10 14:43:12', '2021-11-10', '2021-11-10', 1, 'Abbott'),
(3, 'Nishanth', 'nishanth@coact.co.in', 'Bengaluru', 'cuibxj', NULL, NULL, '2021/11/11 15:05:03', '2021-11-11', '2021-11-11', 1, 'Abbott'),
(4, 'Abhinav Dwivedi', 'abhinav.dwivedi@abbott.com', 'Hyderabad', 'AVD', NULL, NULL, '2021/11/11 18:01:00', '2021-11-11', '2021-11-11', 1, 'Abbott'),
(5, 'Udit Mathur', 'udit_m.in@jtbap.com', 'Gurgaon', 'Test', NULL, NULL, '2021/11/11 18:25:36', '2021-11-11', '2021-11-11', 1, 'Abbott'),
(6, 'Abhinav Dwivedi', 'abhinav.dwivedi@abbott.com', 'Hyderabad', 'AVD', NULL, NULL, '2021/11/11 18:26:31', '2021-11-11', '2021-11-11', 1, 'Abbott'),
(7, 'RAVI KIRAN BANDARI', 'ravikiran29@gmail.com', 'Hyderabad', 'Abbott', NULL, NULL, '2021/11/11 18:27:44', '2021-11-11', '2021-11-11', 1, 'Abbott'),
(8, 'PLNKapardhi', 'dr.kaparthi@gmail.com', 'Hyderabad', 'care hospitals', NULL, NULL, '2021/11/11 18:42:02', '2021-11-11', '2021-11-11', 1, 'Abbott'),
(9, 'RAVI KIRAN BANDARI', 'ravikiran.bandari@abbott.com', 'Hyderabad', 'Abbott', NULL, NULL, '2021/11/11 18:42:20', '2021-11-11', '2021-11-11', 1, 'Abbott'),
(10, 'Dr K Gopala Krishna', 'gopalkoduru@gmail.com', 'vijayawada', 'Aayush Hospitals', NULL, NULL, '2021/11/11 18:46:31', '2021-11-11', '2021-11-11', 1, 'Abbott'),
(11, 'jabir', 'drjabi@yahoo.co.in', 'Kochi', 'lisie', NULL, NULL, '2021/11/11 18:54:56', '2021-11-11', '2021-11-11', 1, 'Abbott'),
(12, 'Sravan ', 'sravan.kumar@abbott.com', 'Hyderabad ', 'Av', NULL, NULL, '2021/11/11 18:55:48', '2021-11-11', '2021-11-11', 1, 'Abbott'),
(13, 'Bharat  V Purohit', 'dr.bvp1@gmail.com', 'Hyderabad', 'Care hospital Hitech city Hyderabad', NULL, NULL, '2021/11/11 18:58:50', '2021-11-11', '2021-11-11', 1, 'Abbott'),
(14, 'Sujesh V ', 'sujesh.vs@abbott.com', 'Cochin  ', 'Abbott Vascular', NULL, NULL, '2021/11/11 19:00:43', '2021-11-11', '2021-11-11', 1, 'Abbott'),
(15, 'Sudheer', 'sudheerkoganti129@gmail.com', 'Hyderabad', 'Citizens', NULL, NULL, '2021/11/11 19:00:45', '2021-11-11', '2021-11-11', 1, 'Abbott'),
(16, 'sujatha vipperla', 'sujasri@hotmail.com', 'visakhapatnam', 'Indus hospitals', NULL, NULL, '2021/11/11 19:00:49', '2021-11-11', '2021-11-11', 1, 'Abbott'),
(17, 'SRIDHAR KASTURI', 'sridharkasturi@yahoo.com', 'hyderabad', 'Sunshine hospital', NULL, NULL, '2021/11/11 19:02:14', '2021-11-11', '2021-11-11', 1, 'Abbott'),
(18, 'Bharat Purohit', 'dr.bvp1@gmail.com', 'Hyderabad', 'Care hospital Hitech city Hyderabad', NULL, NULL, '2021/11/11 19:21:53', '2021-11-11', '2021-11-11', 1, 'Abbott'),
(19, 'RAVI KIRAN BANDARI', 'ravikiran.bandari@abbott.com', 'Hyderabad', 'Abbott', NULL, NULL, '2021/11/11 20:22:52', '2021-11-11', '2021-11-11', 1, 'Abbott'),
(20, 'Nishanth', 'nishanth@coact.co.in', 'Bengaluru', 'cuibxj', NULL, NULL, '2021/11/16 00:17:26', '2021-11-16', '2021-11-16', 1, 'Abbott');

-- --------------------------------------------------------

--
-- Table structure for table `faculty_new`
--

CREATE TABLE `faculty_new` (
  `id` int(50) NOT NULL,
  `Name` varchar(200) NOT NULL,
  `EmailID` varchar(200) NOT NULL,
  `City` varchar(200) NOT NULL,
  `Hospital` varchar(200) NOT NULL,
  `Last Login On` varchar(200) NOT NULL,
  `Logout Times` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_questions`
--

CREATE TABLE `tbl_questions` (
  `id` int(11) NOT NULL,
  `user_name` varchar(255) NOT NULL,
  `user_email` varchar(255) NOT NULL,
  `user_question` varchar(500) NOT NULL,
  `asked_at` datetime NOT NULL,
  `eventname` varchar(255) NOT NULL,
  `speaker` int(11) NOT NULL DEFAULT '0',
  `answered` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_user`
--

CREATE TABLE `tbl_user` (
  `id` int(11) NOT NULL,
  `user_name` varchar(255) NOT NULL,
  `user_email` varchar(500) NOT NULL,
  `city` varchar(100) NOT NULL,
  `hospital` varchar(100) NOT NULL,
  `speciality` varchar(100) DEFAULT NULL,
  `reg_no` varchar(50) DEFAULT NULL,
  `joining_date` datetime NOT NULL,
  `login_date` datetime DEFAULT NULL,
  `logout_date` datetime DEFAULT NULL,
  `logout_status` int(11) NOT NULL DEFAULT '0' COMMENT '1=loggedin',
  `eventname` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_users`
--

CREATE TABLE `tbl_users` (
  `id` int(11) NOT NULL,
  `user_name` varchar(255) NOT NULL,
  `user_email` varchar(500) NOT NULL,
  `city` varchar(100) NOT NULL,
  `hospital` varchar(100) NOT NULL,
  `speciality` varchar(100) DEFAULT NULL,
  `reg_no` varchar(50) DEFAULT NULL,
  `joining_date` datetime NOT NULL,
  `login_date` datetime DEFAULT NULL,
  `logout_date` datetime DEFAULT NULL,
  `logout_status` int(11) NOT NULL DEFAULT '0' COMMENT '1=loggedin',
  `eventname` varchar(255) NOT NULL,
  `token` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_users`
--

INSERT INTO `tbl_users` (`id`, `user_name`, `user_email`, `city`, `hospital`, `speciality`, `reg_no`, `joining_date`, `login_date`, `logout_date`, `logout_status`, `eventname`, `token`) VALUES
(1, 'Nishanth', 'nishanth@coact.co.in', 'Bengaluru', 'cuibxj', NULL, NULL, '2021-11-09 13:01:02', '2021-11-09 13:01:02', '2021-11-09 13:01:08', 0, 'Abbott', '0ee54dd678952dd02d3dbb9ef56f8df6'),
(2, 'Vibhav ', 'vibhav@coact.co.in', 'Pune', 'ABC', NULL, NULL, '2021-11-10 14:33:23', '2021-11-10 14:33:23', '2021-11-10 14:35:05', 0, 'Abbott', 'fc372688ac85062c6c53f09600b0eb83'),
(3, 'Vibhav ', 'vibhav@coact.co.in', 'Pune', 'ABC', NULL, NULL, '2021-11-10 14:42:51', '2021-11-10 14:42:51', '2021-11-10 14:43:00', 0, 'Abbott', '365d11e3312985f28ddb3369da7e7c09'),
(4, 'Pooja', 'pooja@coact.co.in', 'Mumbaj', 'Na', NULL, NULL, '2021-11-10 16:26:12', '2021-11-10 16:26:12', '2021-11-10 16:26:17', 0, 'Abbott', '60d9663166d2195e5e23b44188dd01a8'),
(5, 'Abhinav Dwivedi', 'abhinav.dwivedi@abbott.com', 'Hyderabad', 'AVD', NULL, NULL, '2021-11-10 17:56:32', '2021-11-10 17:56:32', '2021-11-10 19:26:32', 0, 'Abbott', 'e0a749f0aacf8b1be233ccbabf6aab28'),
(6, 'Charan', 'charanparuchuri@gmail.com', 'Hyderabad ', 'CARE HOSPITAL ', NULL, NULL, '2021-11-11 10:20:37', '2021-11-11 10:20:37', '2021-11-11 11:50:37', 0, 'Abbott', 'd0e14e4753c37727a37bed492c36b786'),
(7, 'Nishanth', 'nishanth@coact.co.in', 'Bengaluru', 'cuibxj', NULL, NULL, '2021-11-11 11:56:29', '2021-11-11 11:56:29', '2021-11-11 13:26:29', 0, 'Abbott', '69be4db37dafae71aa3cb000a4fa8cff'),
(8, 'Chiranjeevi M', 'chirumadhagoni@gmail.com', 'Hyderabad ', 'Yashoda Hospital secunderabad ', NULL, NULL, '2021-11-11 13:14:38', '2021-11-11 13:14:38', '2021-11-11 14:44:38', 0, 'Abbott', '62fb15a4bdf06250828b1668f60f8578'),
(9, 'Anil Kumar Mahapatro', 'anil.mahapatro@gmail.com', 'Visakhapatnam', 'Indus Hospital', NULL, NULL, '2021-11-11 13:22:27', '2021-11-11 13:22:27', '2021-11-11 14:52:27', 0, 'Abbott', 'c14152a18d81db8d21b4a3c4d2e894ac'),
(10, 'Nishanth', 'nishanth@coact.co.in', 'Bengaluru', 'cuibxj', NULL, NULL, '2021-11-11 15:06:22', '2021-11-11 15:06:22', '2021-11-11 15:06:46', 0, 'Abbott', '04063d4dcd6f87bd90828cd6e56473e7'),
(11, 'Sivadayal K', 'sivadayalk@yahoo.com', 'Visakhapatnam ', 'Indus ', NULL, NULL, '2021-11-11 18:49:32', '2021-11-11 18:49:32', '2021-11-11 20:19:32', 1, 'Abbott', '4e7162e25b8af81f2900f913d40326e7'),
(12, 'Kishore V R Akella', 'kishore.akella@abbott.com', 'Vizag', 'AV', NULL, NULL, '2021-11-11 18:58:39', '2021-11-11 18:58:39', '2021-11-11 20:28:39', 1, 'Abbott', 'd1d8cea6e45addff41a5668c0005b38d'),
(13, 'sujatha vipperla', 'sujasri@hotmail.com', 'visakhapatnam', 'Indus hospitals', NULL, NULL, '2021-11-11 18:58:42', '2021-11-11 18:58:42', '2021-11-11 20:28:42', 1, 'Abbott', '2d4d637d83d08baf90393a6abb266065'),
(14, 'rohini', 'rohini@gmail.com', 'vijayawada', 'aayysh', NULL, NULL, '2021-11-11 19:00:09', '2021-11-11 19:00:09', '2021-11-11 20:30:09', 1, 'Abbott', '9719ab39fd3e20c4faee13c397476215'),
(15, 'Sujesh V S', 'sujesh.vs@abbott.com', 'Cochin ', 'Lisie ', NULL, NULL, '2021-11-11 19:01:59', '2021-11-11 19:01:59', '2021-11-11 20:31:59', 1, 'Abbott', 'ccafc383aaecfff7bb015cacb7f2276b'),
(16, 'Anil Kumar Mahapatro', 'anil.mahapatro@gmail.com', 'Visakhapatnam', 'Indus Hospital', NULL, NULL, '2021-11-11 19:02:47', '2021-11-11 19:02:47', '2021-11-11 20:32:47', 1, 'Abbott', '0c598722a7861234636cbfcd648537ab'),
(17, 'Ramakrishna reddy ', 'rkreddyjitta@gmail.com', 'Hyderabad ', 'Care hospital ', NULL, NULL, '2021-11-11 19:03:33', '2021-11-11 19:03:33', '2021-11-11 19:04:23', 0, 'Abbott', '399b3e6222deb7028af8eba7b0695909'),
(18, 'Sivadayal K', 'sivadayalk@yahoo.com', 'Visakhapatnam ', 'Indus ', NULL, NULL, '2021-11-11 19:04:35', '2021-11-11 19:04:35', '2021-11-11 20:34:35', 1, 'Abbott', 'b70998770adbd7d35e178eaa96127e28'),
(19, 'Abhinav Dwivedi', 'abhinav.dwivedi@abbott.com', 'Hyderabad', 'AVD', NULL, NULL, '2021-11-11 19:04:40', '2021-11-11 19:04:40', '2021-11-11 20:34:40', 1, 'Abbott', '13b01801e6c23d3b035c8b1d86112759'),
(20, 'Sravan', 'sravan.kumar@abbott.com', 'Hyderabad ', 'Av', NULL, NULL, '2021-11-11 19:06:02', '2021-11-11 19:06:02', '2021-11-11 20:36:02', 1, 'Abbott', '72ebcc84a26ab092e1f3b4c29946b327'),
(21, 'Anil Chopra', 'anil.chopra@abbott.com', 'Hyderabad', 'AV', NULL, NULL, '2021-11-11 19:06:39', '2021-11-11 19:06:39', '2021-11-11 20:36:39', 0, 'Abbott', '5eae2703b74e58a99f0f61d714475ec8'),
(22, 'Pavan Kumar', 'pavankumar.rallapalli@abbott.com', 'Hyderabad', 'Abbott', NULL, NULL, '2021-11-11 19:07:54', '2021-11-11 19:07:54', '2021-11-11 20:37:54', 0, 'Abbott', '15171a4333d89946210cb343a71bce3c'),
(23, 'SARATH S K', 'sarathsksjm@gmail.com', 'TRIVANDRUM', 'Abbott', NULL, NULL, '2021-11-11 19:09:07', '2021-11-11 19:09:07', '2021-11-11 20:39:07', 0, 'Abbott', 'b4485a888ca896cb2cdff0eaaef91274'),
(24, 'kishore A', 'kishore.akella@abbott.com', 'Visakhapatnam', 'Abbott', NULL, NULL, '2021-11-11 19:14:25', '2021-11-11 19:14:25', '2021-11-11 20:44:25', 0, 'Abbott', '6e123d1521fa3a86bc8796a32bb59a98'),
(25, 'Nirmal Kumar', 'drnirmal_kumar@yahoo.com', 'Hyderabad', 'Care Hospital', NULL, NULL, '2021-11-11 19:15:00', '2021-11-11 19:15:00', '2021-11-11 20:45:00', 0, 'Abbott', '5861200c237b245a74eebb539e2078fd'),
(26, 'Udit Mathur', 'udit_m.in@jtbap.com', 'Gurgaon', 'Jtb', NULL, NULL, '2021-11-11 19:15:38', '2021-11-11 19:15:38', '2021-11-11 20:45:38', 0, 'Abbott', 'cad8f1786e7214147163b88bab1affd6'),
(27, 'Ramakrishna reddy ', 'rkreddyjitta@gmail.com', 'Hyderabad ', 'Care hospital ', NULL, NULL, '2021-11-11 19:16:05', '2021-11-11 19:16:05', '2021-11-11 19:57:46', 0, 'Abbott', '2317c42b21384abbed6439f4f1be5dbf'),
(28, 'Dr Ravi Sangolkar', 'ravi.sangolkar@gmail.com', 'Hyderabad', 'Care Hospital', NULL, NULL, '2021-11-11 19:16:34', '2021-11-11 19:16:34', '2021-11-11 20:46:34', 0, 'Abbott', '2954c9a8dd9def74244445c0189ab51b'),
(29, 'Dr Hanumanth Reddy', 'drhanumanthareddycare@gmail.com', 'Hyderabad', 'Care Hospital', NULL, NULL, '2021-11-11 19:17:27', '2021-11-11 19:17:27', '2021-11-11 20:47:27', 0, 'Abbott', 'b2cea735c729bac692b5ec67b05b10da'),
(30, 'Dr Bhupal', 'bhupal@yahoo.com', 'vijayawada', 'Capital', NULL, NULL, '2021-11-11 19:19:03', '2021-11-11 19:19:03', '2021-11-11 20:49:03', 0, 'Abbott', '8779b44f25b315b54b69d8f5e6e876c3'),
(31, 'Dr Tammiraju', 'ivmrt.raj@homail.com', 'Eluru', 'Ashram hospital', NULL, NULL, '2021-11-11 19:19:37', '2021-11-11 19:19:37', '2021-11-11 20:49:37', 0, 'Abbott', 'c99cf72082563ea161e4abcffd5724ab'),
(32, 'Dr Anil', 'a.anil@htmail.com', 'vijayawada', 'Anu Hospital', NULL, NULL, '2021-11-11 19:20:14', '2021-11-11 19:20:14', '2021-11-11 20:50:14', 0, 'Abbott', '3fe2e565591a7acae84f18438b4ba2c6'),
(33, 'Dr Ravikumar', 'r.kumar@gmai.com', 'vijayawada', 'sentini hosp', NULL, NULL, '2021-11-11 19:20:49', '2021-11-11 19:20:49', '2021-11-11 20:50:49', 0, 'Abbott', '493f4fe954e95cbc2889ddc586994787'),
(34, 'Jetender Jain', 'Jeetu_jain24@yahoo.com', 'Hyderabad', 'NIMS', NULL, NULL, '2021-11-11 19:20:50', '2021-11-11 19:20:50', '2021-11-11 20:50:50', 0, 'Abbott', 'e5b5b9e2e266136bdf277f3b5d0cc01a'),
(35, 'Dr A N Patnaik', 'anpatnaik@yahoo.com', 'Hyderabad', 'Star Hospital', NULL, NULL, '2021-11-11 19:20:56', '2021-11-11 19:20:56', '2021-11-11 20:50:56', 0, 'Abbott', 'fd48df6da0a118f4b7d293d7c618448f'),
(36, 'Dr. Ram', 'dr.ram89@gmail.com', 'Hyderabad ', 'Apollo', NULL, NULL, '2021-11-11 19:21:16', '2021-11-11 19:21:16', '2021-11-11 20:51:16', 0, 'Abbott', 'f2501f3e9b328accc9db1db7683cb105'),
(37, 'Dr raji', 'r.raji@ymail.com', 'vijayawada', 'Kamineni hospital', NULL, NULL, '2021-11-11 19:21:20', '2021-11-11 19:21:20', '2021-11-11 20:51:20', 0, 'Abbott', '55d5a739e99f90710d7ff00200fe1c03'),
(38, 'Dr. Harish', 'harishcardiologynims@gmail.com', 'Hyderabad', 'NIMS', NULL, NULL, '2021-11-11 19:21:38', '2021-11-11 19:21:38', '2021-11-11 20:51:38', 0, 'Abbott', '31a84817dc4d41e705cb2b1e522eab7e'),
(39, 'Dr Deepthi', 'deepthi.kodati@gmail.com', 'Hyderabad', 'Care Hospital', NULL, NULL, '2021-11-11 19:21:39', '2021-11-11 19:21:39', '2021-11-11 20:51:39', 0, 'Abbott', '83d158e8bc4f36cbd21a93e36f466aad'),
(40, 'Dr MSN Pavankumar', 'msnp.kuar@htmil.com', 'vijayawada', 'Kamineni hospital', NULL, NULL, '2021-11-11 19:21:53', '2021-11-11 19:21:53', '2021-11-11 20:51:53', 0, 'Abbott', '8ea2859482ecc86cd6183dd310d06767'),
(41, 'Dr Ashok', 'a.ashok@hhtmail.com', 'vijayawada', 'NRI Hospital', NULL, NULL, '2021-11-11 19:22:32', '2021-11-11 19:22:32', '2021-11-11 20:52:32', 0, 'Abbott', '6b5aab715fc4e5d6110b1b169abeebd7'),
(42, 'Dr Naveen', 'n.naveen@hotmai.com', 'vijayawada', 'Ramesh Hospital', NULL, NULL, '2021-11-11 19:22:59', '2021-11-11 19:22:59', '2021-11-11 20:52:59', 0, 'Abbott', '6e3d2e9157c42e498a63a571bbf7f3fb'),
(43, 'Dr Kamalakar', 'k.kamalakar@ggail.com', 'vijayawada', 'Life Hospital', NULL, NULL, '2021-11-11 19:23:37', '2021-11-11 19:23:37', '2021-11-11 20:53:37', 0, 'Abbott', '88ea0581b0d7b02915b8a235199595a7'),
(44, 'Dr Dasarath', 'd.dasrth@otml.com', 'vijayawada', 'Aayush Hospital', NULL, NULL, '2021-11-11 19:24:11', '2021-11-11 19:24:11', '2021-11-11 20:54:11', 0, 'Abbott', 'd27dd1aa680c827be93ef502fd925df2'),
(45, 'Dr.Gopi Krishna ', 'drgopikrishna44@gmail.com', 'Hyderabad', 'NIMS', NULL, NULL, '2021-11-11 19:24:35', '2021-11-11 19:24:35', '2021-11-11 20:54:35', 0, 'Abbott', '51870977216b78975353757568017c5f'),
(46, 'Dr Sivakumar', 's.kumar@hotmail.com', 'Guntur', 'Uday Hospital', NULL, NULL, '2021-11-11 19:24:42', '2021-11-11 19:24:42', '2021-11-11 20:54:42', 0, 'Abbott', 'a6df56af921cb4959b36a6cd90caa0a0'),
(47, 'Dr Purnanand', 'p.puranand@hotmail.com', 'vijayawada', 'purna heart institute', NULL, NULL, '2021-11-11 19:26:18', '2021-11-11 19:26:18', '2021-11-11 20:56:18', 0, 'Abbott', 'b61645999892489f3058dccb2ca20b6d'),
(48, 'Dr Karthik', 'k.krthik@yamil.com', 'vijayawada', 'Latha hospital', NULL, NULL, '2021-11-11 19:27:09', '2021-11-11 19:27:09', '2021-11-11 20:57:09', 0, 'Abbott', '3b3d868d816b375d24360cfcfdc526b5'),
(49, 'Dr. Naveen', 'naveenjust@gmail.com', 'Hyderabad', 'NIMS', NULL, NULL, '2021-11-11 19:27:44', '2021-11-11 19:27:44', '2021-11-11 20:57:44', 0, 'Abbott', '39442974b4f9b4d459c704b9efa54ed5'),
(50, 'Dr Anjit', 'a.anjit@homail.om', 'Guntur', 'Karumuri hospital', NULL, NULL, '2021-11-11 19:28:04', '2021-11-11 19:28:04', '2021-11-11 20:58:04', 0, 'Abbott', '5486ff855a206367756826924189ecde'),
(51, 'Dr Sanjeev Sengupta', 'drsanjeevsen@gmail.com', 'Hyderabad', 'Military Hospital', NULL, NULL, '2021-11-11 19:28:24', '2021-11-11 19:28:24', '2021-11-11 20:58:24', 0, 'Abbott', 'd97bdbd149ab90e017eb3208a6a2249b'),
(52, 'Dr. Bharat Kumar', 'chukkalambbs21@gmail.com', 'Hyderabad', 'NIMS', NULL, NULL, '2021-11-11 19:28:42', '2021-11-11 19:28:42', '2021-11-11 20:58:42', 0, 'Abbott', 'cf8b6988ed1ab3ba2a0827c6661e9d5e'),
(53, 'Dr Srihari', 'h.srihari@hotmil.com', 'vijayawada', 'Siddhartha medical college', NULL, NULL, '2021-11-11 19:28:45', '2021-11-11 19:28:45', '2021-11-11 20:58:45', 0, 'Abbott', '29827eb51d9731b6b51c85219003cd01'),
(54, 'Dr. Ravi Kanth', 'ravimounikaamshala@gmail.com', 'Hyderabad', 'NIMS', NULL, NULL, '2021-11-11 19:29:18', '2021-11-11 19:29:18', '2021-11-11 20:59:18', 0, 'Abbott', 'f0796ea8b365627311349426988f5a1c'),
(55, 'Dr. Nagendra Kukri', 'nagendra.kukri@gmail.com', 'Hyderabad', 'NIMS', NULL, NULL, '2021-11-11 19:29:57', '2021-11-11 19:29:57', '2021-11-11 20:59:57', 0, 'Abbott', 'fb390fb6357b268d0a5ecc1029798f3a'),
(56, 'Dr. Malleswara Rao', 'mallesh-mbbs-2006@yahoo.com', 'Hyderabad', 'NIMS', NULL, NULL, '2021-11-11 19:30:49', '2021-11-11 19:30:49', '2021-11-11 21:00:49', 0, 'Abbott', '304995d1450d322947082c940ab5798b'),
(57, 'Dr. Suraj Hanumanth', 'suraj.mdpune@gmail.com', 'Hyderabad', 'Care Hospitals', NULL, NULL, '2021-11-11 19:33:42', '2021-11-11 19:33:42', '2021-11-11 21:03:42', 0, 'Abbott', '84c71c444c5c388912fa5f07c9369223'),
(58, 'Dr. lalith Agarwal', 'lalith_agarual_1@yahoo.com', 'Hyderabad', 'Medicover hospitals', NULL, NULL, '2021-11-11 19:35:20', '2021-11-11 19:35:20', '2021-11-11 21:05:20', 0, 'Abbott', '5d49260f5c741b5c732604414a47cd85'),
(59, 'Dr. Prashanth', 'drprashnatvazirani@gmail.com', 'Hyderabad', 'Care Hospitals', NULL, NULL, '2021-11-11 19:36:18', '2021-11-11 19:36:18', '2021-11-11 21:06:18', 0, 'Abbott', '93a4ba37e2743db9d682da3eb309852c'),
(60, 'Dr. Sandeep', 'sandeep_parekh@yahoo.com', 'Hyderabad', 'Care Hospitals', NULL, NULL, '2021-11-11 19:37:23', '2021-11-11 19:37:23', '2021-11-11 21:07:23', 0, 'Abbott', '5aad3413ea6bc11a136cfdae24fd0663'),
(61, 'Dr. Yogesh', 'yjamagek@gmail.com', 'Hyderabad', 'CARE HOSPITALS', NULL, NULL, '2021-11-11 19:38:20', '2021-11-11 19:38:20', '2021-11-11 21:08:20', 0, 'Abbott', '2dba1489eefa0a41ae82fc507ec37806'),
(62, 'DR. RAJESH', 'drrajeshkalyankar@gmail.com', 'HYDERABAD', 'CARE', NULL, NULL, '2021-11-11 19:39:42', '2021-11-11 19:39:42', '2021-11-11 21:09:42', 0, 'Abbott', '48d2dd45e816b54d36576fe8116489e3'),
(63, 'DR. SAKET', 'ksaketh.srivatsav@gmail.com', 'Hyderabad', 'Care Hospitals', NULL, NULL, '2021-11-11 19:40:58', '2021-11-11 19:40:58', '2021-11-11 21:10:58', 0, 'Abbott', '63d4f20acc5537a9bc40a2b935b77224'),
(64, 'Anil Chopra', 'anil.chopra@abbott.com', 'Hyd', 'AV', NULL, NULL, '2021-11-11 19:42:02', '2021-11-11 19:42:02', '2021-11-11 21:12:02', 0, 'Abbott', '467eb9a7e1f98df724ba7c8f4d141539'),
(65, 'Dr Rajaram', 'drrajaramcardio@gmail.com', 'Hyderabad', 'Sunshine hospital', NULL, NULL, '2021-11-11 19:48:06', '2021-11-11 19:48:06', '2021-11-11 21:18:06', 0, 'Abbott', 'e142c48deb9ee9256738f3180f4c8161'),
(66, 'Sivadayal K', 'sivadayalk@yahoo.com', 'Visakhapatnam ', 'Indus', NULL, NULL, '2021-11-11 19:59:00', '2021-11-11 19:59:00', '2021-11-11 21:29:00', 0, 'Abbott', 'd35bc2d420e750f7259be9054856e2fb'),
(67, 'Dr.Kamal', 'dr.kamal@yahoo.co.in', 'Kerala', 'Cochi cardiac', NULL, NULL, '2021-11-11 20:13:27', '2021-11-11 20:13:27', '2021-11-11 21:43:27', 0, 'Abbott', '6f542e0751de275744bc38585a1abb9e'),
(68, 'Abhinav Dwivedi', 'abhinav.dwivedi@abbott.com', 'Hyderabad', 'AVD', NULL, NULL, '2021-11-11 20:16:48', '2021-11-11 20:16:48', '2021-11-11 21:46:48', 0, 'Abbott', 'daa75ecd714699d634205a6cc74aadeb'),
(69, 'Ram', 'dr.ram@gmail.com', 'Hy', 'H', NULL, NULL, '2021-11-11 20:17:53', '2021-11-11 20:17:53', '2021-11-11 21:47:53', 0, 'Abbott', '62b342472bb75939782ded4243a47fd2'),
(70, 'Abhinav Dwivedi', 'abhinav.dwivedi@abbott.com', 'Hyderabad', 'AVD', NULL, NULL, '2021-11-11 20:18:16', '2021-11-11 20:18:16', '2021-11-11 21:48:16', 0, 'Abbott', 'a108d2fd5fdf8b269a5953333760332b'),
(71, 'Udit Mathur', 'udit_m.in@jtbap.com', 'Gurgaon', 'Jtb', NULL, NULL, '2021-11-11 20:36:17', '2021-11-11 20:36:17', '2021-11-11 21:00:01', 0, 'Abbott', 'e013a0cf9a88d6ba5996d464f530ed02'),
(72, 'Abhinav Dwivedi', 'abhinav.dwivedi@abbott.com', 'Hyderabad', 'AVD', NULL, NULL, '2021-11-11 21:50:55', '2021-11-11 21:50:55', '2021-11-11 23:20:55', 0, 'Abbott', '4236b302e2173cc594193d41c7bc30ec'),
(73, 'Nishanth', 'nishanth@coact.co.in', 'Bengaluru', 'cuibxj', NULL, NULL, '2021-11-14 16:13:53', '2021-11-14 16:13:53', '2021-11-14 17:43:53', 1, 'Abbott', 'b365ea6254828ab1672c8e716257ceb6'),
(74, 'Nishanth', 'nishanth@coact.co.in', 'Bengaluru', 'cuibxj', NULL, NULL, '2021-11-16 00:17:40', '2021-11-16 00:17:40', '2021-11-16 01:47:40', 1, 'Abbott', '1092683a6415454914762df8d8ed8ae5'),
(75, 'srinivasa reddy', 'karumuri63@gmail.com', 'guntur', 'kssh', NULL, NULL, '2021-11-17 13:19:56', '2021-11-17 13:19:56', '2021-11-17 14:49:56', 1, 'Abbott', 'f3adc1bf410d0281ccd4021978162f84'),
(76, 'Nishanth', 'nishanth@coact.co.in', 'Bengaluru', 'cuibxj', NULL, NULL, '2021-11-19 23:54:36', '2021-11-19 23:54:36', '2021-11-19 23:57:05', 0, 'Abbott', '683464fbd2883cc4f930d8b4415918dd');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `faculty`
--
ALTER TABLE `faculty`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `faculty_new`
--
ALTER TABLE `faculty_new`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_questions`
--
ALTER TABLE `tbl_questions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_user`
--
ALTER TABLE `tbl_user`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_users`
--
ALTER TABLE `tbl_users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `faculty`
--
ALTER TABLE `faculty`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
--
-- AUTO_INCREMENT for table `faculty_new`
--
ALTER TABLE `faculty_new`
  MODIFY `id` int(50) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_questions`
--
ALTER TABLE `tbl_questions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_user`
--
ALTER TABLE `tbl_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_users`
--
ALTER TABLE `tbl_users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=77;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
