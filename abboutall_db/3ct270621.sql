-- phpMyAdmin SQL Dump
-- version 4.6.6deb5ubuntu0.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Jan 14, 2022 at 08:56 AM
-- Server version: 5.7.36-0ubuntu0.18.04.1
-- PHP Version: 7.2.24-0ubuntu0.18.04.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `3ct270621`
--

-- --------------------------------------------------------

--
-- Table structure for table `faculty`
--

CREATE TABLE `faculty` (
  `id` int(11) NOT NULL,
  `user_name` varchar(200) NOT NULL,
  `user_email` varchar(200) NOT NULL,
  `city` varchar(100) NOT NULL,
  `hospital` varchar(100) NOT NULL,
  `speciality` varchar(100) DEFAULT NULL,
  `reg_no` varchar(100) DEFAULT NULL,
  `joining_date` varchar(100) NOT NULL,
  `login_date` date DEFAULT NULL,
  `logout_date` date DEFAULT NULL,
  `logout_status` int(11) NOT NULL DEFAULT '0' COMMENT '1 = logged in',
  `eventname` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `faculty_new`
--

CREATE TABLE `faculty_new` (
  `id` int(50) NOT NULL,
  `Name` varchar(200) NOT NULL,
  `EmailID` varchar(200) NOT NULL,
  `City` varchar(200) NOT NULL,
  `Hospital` varchar(200) NOT NULL,
  `Last Login On` varchar(200) NOT NULL,
  `Logout Times` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_questions`
--

CREATE TABLE `tbl_questions` (
  `id` int(11) NOT NULL,
  `user_name` varchar(255) NOT NULL,
  `user_email` varchar(255) NOT NULL,
  `user_question` varchar(500) NOT NULL,
  `asked_at` datetime NOT NULL,
  `eventname` varchar(255) NOT NULL,
  `speaker` int(11) NOT NULL DEFAULT '0',
  `answered` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_user`
--

CREATE TABLE `tbl_user` (
  `id` int(11) NOT NULL,
  `user_name` varchar(255) NOT NULL,
  `user_email` varchar(500) NOT NULL,
  `city` varchar(100) NOT NULL,
  `hospital` varchar(100) NOT NULL,
  `speciality` varchar(100) DEFAULT NULL,
  `reg_no` varchar(50) DEFAULT NULL,
  `joining_date` datetime NOT NULL,
  `login_date` datetime DEFAULT NULL,
  `logout_date` datetime DEFAULT NULL,
  `logout_status` int(11) NOT NULL DEFAULT '0' COMMENT '1=loggedin',
  `eventname` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_users`
--

CREATE TABLE `tbl_users` (
  `id` int(11) NOT NULL,
  `user_name` varchar(255) NOT NULL,
  `user_email` varchar(500) NOT NULL,
  `city` varchar(100) NOT NULL,
  `hospital` varchar(100) NOT NULL,
  `speciality` varchar(100) DEFAULT NULL,
  `reg_no` varchar(50) DEFAULT NULL,
  `joining_date` datetime NOT NULL,
  `login_date` datetime DEFAULT NULL,
  `logout_date` datetime DEFAULT NULL,
  `logout_status` int(11) NOT NULL DEFAULT '0' COMMENT '1=loggedin',
  `eventname` varchar(255) NOT NULL,
  `token` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_users`
--

INSERT INTO `tbl_users` (`id`, `user_name`, `user_email`, `city`, `hospital`, `speciality`, `reg_no`, `joining_date`, `login_date`, `logout_date`, `logout_status`, `eventname`, `token`) VALUES
(1, 'Nishanth.S', 'nishanth@coact.co.in', 'bangalore', 'bangl', NULL, NULL, '2021-07-05 22:15:54', '2021-07-05 22:15:54', '2021-07-05 22:16:00', 0, 'Abbott', '4690fc35da5a860a8b553bbe3b78b68a'),
(2, 'Nishanth.S', 'nishanth@coact.co.in', 'bangalore', 'bangl', NULL, NULL, '2021-07-05 22:19:03', '2021-07-05 22:19:03', '2021-07-05 22:19:09', 0, 'Abbott', 'fad974cccb7f3df14393fab68323e550'),
(3, 'Nishanth.S', 'nishanth@coact.co.in', 'bangalore', 'bangl', NULL, NULL, '2021-07-06 18:12:54', '2021-07-06 18:12:54', '2021-07-06 18:13:12', 0, 'Abbott', 'b4b31f746fc49d05c38779c4dd2a9491'),
(4, 'CHANDRADSEKARAN KOLANDAISAMY', 'kcpscg@gmail.com', 'chennai', 'Prashanth superspeciality hospital', NULL, NULL, '2021-07-06 19:15:44', '2021-07-06 19:15:44', '2021-07-06 20:45:44', 0, 'Abbott', '7fd379c011c7e23c5d51166b808ffc06'),
(5, 'Muralidharan T R', 'muralidharantr@gmail.com', 'CHENNAI', 'SRIHER', NULL, NULL, '2021-07-06 19:23:26', '2021-07-06 19:23:26', '2021-07-06 20:53:26', 0, 'Abbott', '0b3c7b16b755a0640a7709091148809f'),
(6, 'K.Jaishankar', 'drkjs68@yahoo.com', 'Chennai', 'MIOT', NULL, NULL, '2021-07-06 19:24:19', '2021-07-06 19:24:19', '2021-07-06 20:54:19', 0, 'Abbott', '32bec7f658f857fb6b3aa70c4f10b975'),
(7, 'Udit', 'udit_m.in@jtbap.com', 'Test', 'Test', NULL, NULL, '2021-07-06 19:24:25', '2021-07-06 19:24:25', '2021-07-06 20:54:25', 0, 'Abbott', '9a6c9c0c96148809189ba04e48cd9d6e'),
(8, 'lawrance jesuraj', 'drjesuraj@gmail.com', 'COIMBATORE', 'kmch hospitals', NULL, NULL, '2021-07-06 19:26:34', '2021-07-06 19:26:34', '2021-07-06 20:56:34', 0, 'Abbott', 'd669258c9776fd5aaad6c49c509863e8'),
(9, 'Udit', 'udit_m.in@jtbap.com', 'Test', 'Test', NULL, NULL, '2021-07-06 19:29:21', '2021-07-06 19:29:21', '2021-07-06 20:59:21', 0, 'Abbott', '992228adc4f38d08ac5b75b3602ed00d'),
(10, 'dr.guru', 'drguru73@yahoo.com', 'chennai', 'miot', NULL, NULL, '2021-07-06 19:30:35', '2021-07-06 19:30:35', '2021-07-06 21:00:35', 0, 'Abbott', '900ff58a9f7cf5b0d8e0ae91748fd1f3'),
(11, 'A.rohit', 'a.rohit@abbott.com', 'Coimbatore', 'Av', NULL, NULL, '2021-07-06 19:33:47', '2021-07-06 19:33:47', '2021-07-06 21:03:47', 0, 'Abbott', '6e068919fc5db6176acef87104be2dd5'),
(12, 'Rajesh', 'rajeshkumarmd@gmail.com', 'Chennai', 'Prashanth Hospital ', NULL, NULL, '2021-07-06 19:35:16', '2021-07-06 19:35:16', '2021-07-06 21:05:16', 0, 'Abbott', 'ac25199fe391790239fdc4befe254389'),
(13, 'Nishanth.S', 'nishanth@coact.co.in', 'bangalore', 'bangl', NULL, NULL, '2021-07-06 19:36:13', '2021-07-06 19:36:13', '2021-07-06 19:36:19', 0, 'Abbott', 'a951ec09831d4591194258419594ffad'),
(14, 'Shiyam', 'shiyam.sundar@abbott.com', 'Chennai', 'AV', NULL, NULL, '2021-07-06 19:37:21', '2021-07-06 19:37:21', '2021-07-06 21:07:21', 0, 'Abbott', '8357d43d8f148a9a38064ca1dbbffb61'),
(15, 'Shiyam ', 'Shiyam.sundar@abbott.com', 'Chennai', 'AV', NULL, NULL, '2021-07-06 19:38:59', '2021-07-06 19:38:59', '2021-07-06 21:08:59', 0, 'Abbott', 'd26a26aaacbb78d887382be58f7395d7'),
(16, 'Ashok', 'gashokmkumar@gmail.com', 'Chennai', 'Chettinad Hospital ', NULL, NULL, '2021-07-06 19:39:07', '2021-07-06 19:39:07', '2021-07-06 21:09:07', 0, 'Abbott', '9e0eea093edb593be09709658efa647e'),
(17, 'Radha Priya', 'Radhapri@gmail.com', 'Chennai', 'Apollo Hospital ', NULL, NULL, '2021-07-06 19:39:42', '2021-07-06 19:39:42', '2021-07-06 21:09:42', 0, 'Abbott', '35210ef554cc99a7263c18de68d47f9f'),
(18, 'Giridharan ', 'Girigirias@gmail.com', 'Pondicherry', 'MGM', NULL, NULL, '2021-07-06 19:40:17', '2021-07-06 19:40:17', '2021-07-06 21:10:17', 0, 'Abbott', '361ca7203ec1d68118b978abb9e461e0'),
(19, 'Karthikeyan ', 'Karthikeyandm@gmail.com', 'Chennai', 'Chennai', NULL, NULL, '2021-07-06 19:41:41', '2021-07-06 19:41:41', '2021-07-06 21:11:41', 0, 'Abbott', '052811bfa3ca255397fc0d281a12d129'),
(20, 'Govind', 'govindaraj.kamaraj@abbott.com', 'Chennai', 'Abbott Vascular', NULL, NULL, '2021-07-06 19:41:47', '2021-07-06 19:41:47', '2021-07-06 21:11:47', 0, 'Abbott', 'e1d8d61e371d3a8761d0c246b22ea550'),
(21, 'Asuthosh', 'Asutoshmd@gmail.com', 'Pondicherry', 'JIPMER', NULL, NULL, '2021-07-06 19:42:13', '2021-07-06 19:42:13', '2021-07-06 21:12:13', 0, 'Abbott', '4b52862d4b8528d6929b27ad79deac24'),
(22, 'Dr.Karthik', 'karthikkannan@gmail.com', 'Chennai', 'Apollo', NULL, NULL, '2021-07-06 19:43:04', '2021-07-06 19:43:04', '2021-07-06 21:13:04', 0, 'Abbott', '036f39dddab4307324789e8f351b5e14'),
(23, 'Srinivasan K', 'drsrinik@gmail.com', 'Chennai 600020', 'CSSH', NULL, NULL, '2021-07-06 19:46:16', '2021-07-06 19:46:16', '2021-07-06 20:22:37', 0, 'Abbott', 'f3cba374dea7b002019ba2a6185e43c7'),
(24, 'Udit', 'udit_m.in@jtbap.com', 'Test', 'Test', NULL, NULL, '2021-07-06 19:46:22', '2021-07-06 19:46:22', '2021-07-06 21:16:22', 0, 'Abbott', '88861b24adaf33241612e144574c1b85'),
(25, 'Dr santhosh ', 'drsanthosh1991@gmail.com', 'Chennai ', 'Srmc ', NULL, NULL, '2021-07-06 19:46:43', '2021-07-06 19:46:43', '2021-07-06 21:16:43', 0, 'Abbott', '5c23385098186ce38d1f097c562c8f33'),
(26, 'Gowtham', 'rajigowthaman@gmail.com', 'Chennai', 'Abbott', NULL, NULL, '2021-07-06 19:47:10', '2021-07-06 19:47:10', '2021-07-06 21:17:10', 0, 'Abbott', '81df100f380099d0066b56687719351d'),
(27, 'Vinoth', 'vinoth1998@gmail.com', 'Chennai', 'Kaveri', NULL, NULL, '2021-07-06 19:47:53', '2021-07-06 19:47:53', '2021-07-06 21:17:53', 0, 'Abbott', 'fd38fde616afbecb584108b09bb9abc1'),
(28, 'Aditya', 'adityavruia@gmail.com', 'Chennai ', 'SRMC', NULL, NULL, '2021-07-06 19:48:12', '2021-07-06 19:48:12', '2021-07-06 21:18:12', 0, 'Abbott', 'dbc79e9a3482b2c9f71bfa7d1e33f9ff'),
(29, 'Daniel K', 'danielchristoper1998@gmail.com', 'Chennai', 'Abbott', NULL, NULL, '2021-07-06 19:48:35', '2021-07-06 19:48:35', '2021-07-06 21:18:35', 0, 'Abbott', '5d1f314c783abf1f107e9c51713b7838'),
(30, 'Prakash', 'prakash12@gmail.com', 'Chennai', '.', NULL, NULL, '2021-07-06 19:48:54', '2021-07-06 19:48:54', '2021-07-06 21:18:54', 0, 'Abbott', 'c0d23ce208656757f5bcb9edf5b9bcbf'),
(31, 'Rahul', 'rahul.kongara@gmail.com', 'Chennai ', 'SRMC', NULL, NULL, '2021-07-06 19:49:10', '2021-07-06 19:49:10', '2021-07-06 21:19:10', 0, 'Abbott', '208fd2d09c57fbd019c700a66cee9ef3'),
(32, 'dr santhosh', 'drsanthosh1991@gmail.com', 'chennai', 'srmc', NULL, NULL, '2021-07-06 19:49:55', '2021-07-06 19:49:55', '2021-07-06 21:19:55', 0, 'Abbott', 'd124ed70a0bb3586a23ca6767d63f3ec'),
(33, 'Ilayaraja', 'druiraja@yahoo.com', 'Chennai ', 'Billroth', NULL, NULL, '2021-07-06 19:50:05', '2021-07-06 19:50:05', '2021-07-06 21:20:05', 0, 'Abbott', '2193b4b1b59a445e4a9ad1ad8ffd5266'),
(34, 'Ganesh', 'dr.ganesh.t.md@gmail.com', 'Chennai ', 'Evans', NULL, NULL, '2021-07-06 19:50:51', '2021-07-06 19:50:51', '2021-07-06 21:20:51', 0, 'Abbott', 'e2187ccf2fc7d68e4765e88e6dc26eef'),
(35, 'Srikumar', 'ssrk96@yahoo.co.in', 'Chennai ', 'MIOT', NULL, NULL, '2021-07-06 19:51:55', '2021-07-06 19:51:55', '2021-07-06 21:21:55', 0, 'Abbott', '55d621c139378231f951dc43e19640ca'),
(36, 'Shiyam', 'shiyam.sundar@abbott.com', 'Chennai', 'AV', NULL, NULL, '2021-07-06 19:52:09', '2021-07-06 19:52:09', '2021-07-06 21:22:09', 0, 'Abbott', '4f4f4b5a6d52c56ebc27bb973964da54'),
(37, 'Ashok', 'drashokkumar87@gmail.com', 'Chennai ', 'Kamakshi', NULL, NULL, '2021-07-06 19:52:35', '2021-07-06 19:52:35', '2021-07-06 21:22:35', 0, 'Abbott', '3d1ec3189e79b8ec9520790fc0409fb9'),
(38, 'Bala', 'bala.balakrish13@gmail.com', 'Chennai ', 'SRMC', NULL, NULL, '2021-07-06 19:53:14', '2021-07-06 19:53:14', '2021-07-06 21:23:14', 0, 'Abbott', 'a2f65ef6db422cd52f8dc07e700fe9c0'),
(39, 'Harish', 'harish2356@gmail.com', 'Chennai', 'Bharathiraja ', NULL, NULL, '2021-07-06 19:54:18', '2021-07-06 19:54:18', '2021-07-06 21:24:18', 0, 'Abbott', '0806b3ee6d042f380e9c0bb190d08cc5'),
(40, 'A Rohit ', 'a.rohit@abbott.com', 'Coimbatore ', 'AV', NULL, NULL, '2021-07-06 19:56:32', '2021-07-06 19:56:32', '2021-07-06 21:26:32', 0, 'Abbott', 'e100da80d89b196c337ca245f484b0c4'),
(41, 'Antony Wilson', 'dr.antonywilson@gmail.com', 'Chennai', 'SRIHER', NULL, NULL, '2021-07-06 20:00:35', '2021-07-06 20:00:35', '2021-07-06 21:30:35', 0, 'Abbott', '513df74c8ec37db30b9305824f51d066'),
(42, 'Joseph', 'Josephtheo84@gmail.com', 'Trichy', 'Kauvery', NULL, NULL, '2021-07-06 20:02:25', '2021-07-06 20:02:25', '2021-07-06 21:32:25', 0, 'Abbott', '74428179d87ca048af49e38302055197'),
(43, 'Ganesh', 'drganeshchc@gmail.com', 'Chennai', 'Chennai', NULL, NULL, '2021-07-06 20:03:30', '2021-07-06 20:03:30', '2021-07-06 21:33:30', 0, 'Abbott', 'f89c43d36c70b000b7f00dcb4459b5d2'),
(44, 'jebaraj', 'jebaraj66@gmail.com', 'chennai', 'SRMC', NULL, NULL, '2021-07-06 20:06:00', '2021-07-06 20:06:00', '2021-07-06 21:36:00', 0, 'Abbott', '43dcb5b1b7265c7c5870fefcb03d22a6'),
(45, 'Vignesh', 'vinoth1998@gmail.com', 'Chennai', '.', NULL, NULL, '2021-07-06 20:06:04', '2021-07-06 20:06:04', '2021-07-06 21:36:04', 0, 'Abbott', 'e99101a879bc750041f8927ad00e59cb'),
(46, 'Sridharan', 'drsridhar87@gmail.com', 'Chennai ', 'MIOT', NULL, NULL, '2021-07-06 20:07:46', '2021-07-06 20:07:46', '2021-07-06 21:37:46', 0, 'Abbott', '7105a0112d44f87ef4f4bf0614e8a1d6'),
(47, 'Sanjay', 'sanjaiinorion@gmail.com', 'Chennai ', 'SIMS', NULL, NULL, '2021-07-06 20:08:42', '2021-07-06 20:08:42', '2021-07-06 21:38:42', 0, 'Abbott', 'b6041c7f0d925758872244a7c127085e'),
(48, 'Aakash', 'dr.aakashtejad@yahoo.com', 'Chennai ', 'SRMC', NULL, NULL, '2021-07-06 20:09:52', '2021-07-06 20:09:52', '2021-07-06 21:39:52', 0, 'Abbott', '1c8aa8d830aa32392700b116b6e3ae9b'),
(49, 'Arun Pari', 'arunparidr@gmail.com', 'Chennai ', 'Bharath', NULL, NULL, '2021-07-06 20:11:02', '2021-07-06 20:11:02', '2021-07-06 21:41:02', 0, 'Abbott', 'cc1865ed0f56de49b9f69fca0161bf38'),
(50, 'Vijay Balaji', 'drvijay89@yahoo.com', 'Chennai ', 'MIOT', NULL, NULL, '2021-07-06 20:11:48', '2021-07-06 20:11:48', '2021-07-06 21:41:48', 0, 'Abbott', '65fdd8f7f0877cdf43ba3a5f4342a563'),
(51, 'David', 'davidchristoper1998@gmail.com', 'Trichi', 'Apollo', NULL, NULL, '2021-07-06 20:25:52', '2021-07-06 20:25:52', '2021-07-06 21:55:52', 0, 'Abbott', 'e39da2f60fd6a29deb1a22f8dbe5caed'),
(52, 'Rosy', 'rosy1678ross@gmail.com', 'Coimbatore', 'KMCH', NULL, NULL, '2021-07-06 20:26:53', '2021-07-06 20:26:53', '2021-07-06 21:56:53', 0, 'Abbott', 'acad13325822d6e501f984f232cbcf2f'),
(53, 'Krishna', 'krishnamurthi@gmail.com', 'Coimbatore', 'PSG', NULL, NULL, '2021-07-06 20:28:26', '2021-07-06 20:28:26', '2021-07-06 21:58:26', 0, 'Abbott', '67b7835bd54906e5e0e3d0cd5eff61ba'),
(54, 'Jaya', 'jayasuriya88@gmail.com', 'Coimbatore', 'KG', NULL, NULL, '2021-07-06 20:29:32', '2021-07-06 20:29:32', '2021-07-06 21:59:32', 0, 'Abbott', '351b31b947a34dc2de8baa886c153fc9'),
(55, 'Arun ', 'arunprakash@gmail.com', 'Selam', 'Government hospital', NULL, NULL, '2021-07-06 20:30:48', '2021-07-06 20:30:48', '2021-07-06 22:00:48', 0, 'Abbott', '9fa3d33f880f3655678bbfd36d970bc5'),
(56, 'Moses', 'mosesjohnson12@gmail.com', 'Coimbatore', 'SRH', NULL, NULL, '2021-07-06 20:31:10', '2021-07-06 20:31:10', '2021-07-06 22:01:10', 0, 'Abbott', 'cc28f1ce6243c57d6fa5d5e8fa2e4fc2'),
(57, 'Madhan', 'madhan89@gmail.com', 'Tirchy', 'Tirchy GH', NULL, NULL, '2021-07-06 20:32:20', '2021-07-06 20:32:20', '2021-07-06 22:02:20', 0, 'Abbott', '70aa9d9576e5fc49c2c3437d6a80a47d'),
(58, 'Dhanush', 'dhanush85@gmail.com', 'Thanjavur', 'Thanjavur GH', NULL, NULL, '2021-07-06 20:33:15', '2021-07-06 20:33:15', '2021-07-06 22:03:15', 0, 'Abbott', '2a438579396b3bb69d0a491aea712ac4'),
(59, 'Balaji', 'bala45@gmail.com', 'Dindigal ', 'Durairaj hospital', NULL, NULL, '2021-07-06 20:38:53', '2021-07-06 20:38:53', '2021-07-06 22:08:53', 0, 'Abbott', '5631f549845b1b02af58d8f28a4f86f7'),
(60, 'Keshevan', 'keshevan78@gmail.com', 'Selam', 'Kauvery', NULL, NULL, '2021-07-06 20:42:16', '2021-07-06 20:42:16', '2021-07-06 22:12:16', 0, 'Abbott', '9caa58170ccd6186ce839a82dc5a9fc6'),
(61, 'Yogeshwaran', 'yogeshyogi@gmail.com', 'Selam', 'Kauvery', NULL, NULL, '2021-07-06 20:45:52', '2021-07-06 20:45:52', '2021-07-06 22:15:52', 0, 'Abbott', 'd6f7ba3b82e2a29903b9b87ad1309443'),
(62, 'Tamilvanan', 'Tamil76@gmail.com', 'Thanjavur', 'Meenakshi', NULL, NULL, '2021-07-06 20:46:51', '2021-07-06 20:46:51', '2021-07-06 22:16:51', 0, 'Abbott', 'baa2914112ba44c8bd5676a342945f7f'),
(63, 'Sathish', 'sathishkumar@gmail.com', 'Thanjavur', 'Meenakshi', NULL, NULL, '2021-07-06 20:47:49', '2021-07-06 20:47:49', '2021-07-06 22:17:49', 0, 'Abbott', 'afb209dcc5579ac7b9c5130ac97a7242'),
(64, 'Balaji', 'balaji78@gmail.com', 'Vellore', 'Apollo KH', NULL, NULL, '2021-07-06 20:50:22', '2021-07-06 20:50:22', '2021-07-06 22:20:22', 0, 'Abbott', '1fcef52d1e2e8a7e2a8e7d66988b16f6'),
(65, 'dr santhosh', 'drsanthosh1991@gmail.com', 'chennai', 'srmc', NULL, NULL, '2021-07-06 20:51:26', '2021-07-06 20:51:26', '2021-07-06 22:21:26', 0, 'Abbott', '67ed6d1352eb6b7ce5ac89a797bea57a'),
(66, 'Udit', 'udit_m.in@jtbap.com', 'Test', 'Test', NULL, NULL, '2021-07-06 21:02:00', '2021-07-06 21:02:00', '2021-07-06 21:18:39', 0, 'Abbott', '8eb26ecadeef750e3b5067d9d090c306'),
(67, 'Daniel K', 'danielchristoper1998@gmail.com', 'Chennai', 'Abbott', NULL, NULL, '2021-07-06 21:34:01', '2021-07-06 21:34:01', '2021-07-06 21:34:09', 0, 'Abbott', 'bf4365a38740c07b24d785ada5ac09bd'),
(68, 'Ramarao Chigurupati', 'drchiguru828@gmail.com', 'Chennai', 'SRMC', NULL, NULL, '2021-07-06 22:41:49', '2021-07-06 22:41:49', '2021-07-06 22:42:16', 0, 'Abbott', 'cf4d220bb742c700ad5463baa7fe87e7'),
(69, 'Sivakumar', 'shiva78.r@gmail.com', 'Chennai (600004)', 'Billroth', NULL, NULL, '2021-07-13 07:35:34', '2021-07-13 07:35:34', '2021-07-13 09:05:34', 0, 'Abbott', 'd6be83c6bacd7e85b48beec967c33a54');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `faculty`
--
ALTER TABLE `faculty`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `faculty_new`
--
ALTER TABLE `faculty_new`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_questions`
--
ALTER TABLE `tbl_questions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_user`
--
ALTER TABLE `tbl_user`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_users`
--
ALTER TABLE `tbl_users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `faculty`
--
ALTER TABLE `faculty`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `faculty_new`
--
ALTER TABLE `faculty_new`
  MODIFY `id` int(50) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_questions`
--
ALTER TABLE `tbl_questions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_user`
--
ALTER TABLE `tbl_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_users`
--
ALTER TABLE `tbl_users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=70;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
