-- phpMyAdmin SQL Dump
-- version 4.6.6deb5ubuntu0.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Jan 14, 2022 at 08:55 AM
-- Server version: 5.7.36-0ubuntu0.18.04.1
-- PHP Version: 7.2.24-0ubuntu0.18.04.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `Angioplasty170921`
--

-- --------------------------------------------------------

--
-- Table structure for table `faculty`
--

CREATE TABLE `faculty` (
  `id` int(11) NOT NULL,
  `user_name` varchar(200) NOT NULL,
  `user_email` varchar(200) NOT NULL,
  `city` varchar(100) NOT NULL,
  `hospital` varchar(100) NOT NULL,
  `speciality` varchar(100) DEFAULT NULL,
  `reg_no` varchar(100) DEFAULT NULL,
  `joining_date` varchar(100) NOT NULL,
  `login_date` date DEFAULT NULL,
  `logout_date` date DEFAULT NULL,
  `logout_status` int(11) NOT NULL DEFAULT '0' COMMENT '1 = logged in',
  `eventname` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `faculty`
--

INSERT INTO `faculty` (`id`, `user_name`, `user_email`, `city`, `hospital`, `speciality`, `reg_no`, `joining_date`, `login_date`, `logout_date`, `logout_status`, `eventname`) VALUES
(1, 'Nishanth', 'support@coact.co.in', 'bangal', 'ban', NULL, NULL, '2021/09/17 19:08:42', '2021-09-17', '2021-09-17', 1, 'Abbott'),
(2, 'Kheerthana ', 'kheerthana_r.in@jtbap.com', 'Bangalore ', 'JTB India', NULL, NULL, '2021/09/17 19:35:50', '2021-09-17', '2021-09-17', 1, 'Abbott'),
(3, 'Kheerthana R', 'kheerthana_r.in@jtbap.com', 'Bangalore', 'JTBindia', NULL, NULL, '2021/09/17 19:39:19', '2021-09-17', '2021-09-17', 1, 'Abbott'),
(4, 'Kheerthana R', 'kheerthana_r.in@jtbap.com', 'Bangalore', 'JTBindia', NULL, NULL, '2021/09/17 19:44:43', '2021-09-17', '2021-09-17', 1, 'Abbott'),
(5, 'SUJESH V S', 'sujesh.vs@abbott.com', 'Cochin', 'Abbott Vascular ', NULL, NULL, '2021/09/17 19:47:09', '2021-09-17', '2021-09-17', 1, 'Abbott'),
(6, 'NAVAJITH V T', 'navajith.thankam@abbott.com', 'KOZHIKODE', 'AV', NULL, NULL, '2021/09/17 19:59:14', '2021-09-17', '2021-09-17', 1, 'Abbott'),
(7, 'jimmy george', 'pjimmygeorge@gmail.com', 'Ernakulam', 'Lisie ', NULL, NULL, '2021/09/17 19:59:30', '2021-09-17', '2021-09-17', 1, 'Abbott'),
(8, 'BINO BENJAMIN', 'binobenjaminc@gmail.com', 'Thrissur', 'Jmmc', NULL, NULL, '2021/09/17 19:59:58', '2021-09-17', '2021-09-17', 1, 'Abbott'),
(9, 'jabir', 'drjabi@yahoo.co.in', 'Kochi', 'lisie', NULL, NULL, '2021/09/17 20:00:43', '2021-09-17', '2021-09-17', 1, 'Abbott'),
(10, 'dr praveen s v', 'drpraveencardio@gmail.com', 'trivandrum', 'kims', NULL, NULL, '2021/09/17 20:00:58', '2021-09-17', '2021-09-17', 1, 'Abbott'),
(11, 'Bino Benjamin', 'binobenjaminc@gmail.com', 'Thrissur', 'jmmc', NULL, NULL, '2021/09/17 20:01:12', '2021-09-17', '2021-09-17', 1, 'Abbott'),
(12, 'Dr Navin Mathew', 'nhcardiology433@gmail.com', 'Kochi', 'Amrita Institute Of Medical Sciences', NULL, NULL, '2021/09/17 20:02:30', '2021-09-17', '2021-09-17', 1, 'Abbott'),
(13, 'Shafiq Rehman', 'drshafiq1978@gmail.com', 'Kochi', 'Renai Medicity', NULL, NULL, '2021/09/17 20:04:10', '2021-09-17', '2021-09-17', 1, 'Abbott'),
(14, 'Dr PRAVEEN S V', 'drpraveencardio@gmail.com', 'Trivandrum', 'KIMS', NULL, NULL, '2021/09/17 20:05:04', '2021-09-17', '2021-09-17', 1, 'Abbott'),
(15, 'SUJESH V S', 'sujesh.vs@abbott.com', 'Cochin', 'Abbott Vascular', NULL, NULL, '2021/09/17 20:54:57', '2021-09-17', '2021-09-17', 1, 'Abbott'),
(16, 'NAVAJITH V T', 'navajith.thankam@abbott.com', 'KOZHIKODE', 'AV', NULL, NULL, '2021/09/17 21:39:10', '2021-09-17', '2021-09-17', 1, 'Abbott');

-- --------------------------------------------------------

--
-- Table structure for table `faculty_new`
--

CREATE TABLE `faculty_new` (
  `id` int(50) NOT NULL,
  `Name` varchar(200) NOT NULL,
  `EmailID` varchar(200) NOT NULL,
  `City` varchar(200) NOT NULL,
  `Hospital` varchar(200) NOT NULL,
  `Last Login On` varchar(200) NOT NULL,
  `Logout Times` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_questions`
--

CREATE TABLE `tbl_questions` (
  `id` int(11) NOT NULL,
  `user_name` varchar(255) NOT NULL,
  `user_email` varchar(255) NOT NULL,
  `user_question` varchar(500) NOT NULL,
  `asked_at` datetime NOT NULL,
  `eventname` varchar(255) NOT NULL,
  `speaker` int(11) NOT NULL DEFAULT '0',
  `answered` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_questions`
--

INSERT INTO `tbl_questions` (`id`, `user_name`, `user_email`, `user_question`, `asked_at`, `eventname`, `speaker`, `answered`) VALUES
(1, 'Bijilesh', 'bijilesh@gmail.com', 'Play the video from original folder', '2021-09-17 21:19:23', 'Abbott', 0, 0),
(2, 'Bijilesh', 'bijilesh@gmail.com', 'Play the original file', '2021-09-17 21:21:19', 'Abbott', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_user`
--

CREATE TABLE `tbl_user` (
  `id` int(11) NOT NULL,
  `user_name` varchar(255) NOT NULL,
  `user_email` varchar(500) NOT NULL,
  `city` varchar(100) NOT NULL,
  `hospital` varchar(100) NOT NULL,
  `speciality` varchar(100) DEFAULT NULL,
  `reg_no` varchar(50) DEFAULT NULL,
  `joining_date` datetime NOT NULL,
  `login_date` datetime DEFAULT NULL,
  `logout_date` datetime DEFAULT NULL,
  `logout_status` int(11) NOT NULL DEFAULT '0' COMMENT '1=loggedin',
  `eventname` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_users`
--

CREATE TABLE `tbl_users` (
  `id` int(11) NOT NULL,
  `user_name` varchar(255) NOT NULL,
  `user_email` varchar(500) NOT NULL,
  `city` varchar(100) NOT NULL,
  `hospital` varchar(100) NOT NULL,
  `speciality` varchar(100) DEFAULT NULL,
  `reg_no` varchar(50) DEFAULT NULL,
  `joining_date` datetime NOT NULL,
  `login_date` datetime DEFAULT NULL,
  `logout_date` datetime DEFAULT NULL,
  `logout_status` int(11) NOT NULL DEFAULT '0' COMMENT '1=loggedin',
  `eventname` varchar(255) NOT NULL,
  `token` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_users`
--

INSERT INTO `tbl_users` (`id`, `user_name`, `user_email`, `city`, `hospital`, `speciality`, `reg_no`, `joining_date`, `login_date`, `logout_date`, `logout_status`, `eventname`, `token`) VALUES
(1, 'Nishanth', 'nishanth@coact.co.in', 'bangal', 'ban', NULL, NULL, '2021-09-17 18:42:50', '2021-09-17 18:42:50', '2021-09-17 18:57:52', 0, 'Abbott', 'fd502b9b9be61ab5b0a5ae04118353ae'),
(2, 'Nishanth', 'support@coact.co.in', 'bangal', 'ban', NULL, NULL, '2021-09-17 19:10:46', '2021-09-17 19:10:46', '2021-09-17 19:10:53', 0, 'Abbott', 'cadde6b35c67e3bae625517bfc33fd4d'),
(3, 'Ritesh R', 'dr_r_ritesh@yahoo.com', 'Kattapana', 'St Johns Hospital', NULL, NULL, '2021-09-17 19:42:12', '2021-09-17 19:42:12', '2021-09-17 21:12:12', 0, 'Abbott', '7532cdb744a5cec272e15850e9d55c58'),
(4, 'Lijo ', 'lijo.k@abbott.com', 'Cochin ', 'Abbott ', NULL, NULL, '2021-09-17 19:52:23', '2021-09-17 19:52:23', '2021-09-17 21:22:23', 0, 'Abbott', '40c11b07e44e24142a1de7ba7393e790'),
(5, 'PAVAN KUMAR', 'pavankumar.rallapalli@abbott.com', 'Hyderabad', 'Abbott', NULL, NULL, '2021-09-17 19:53:13', '2021-09-17 19:53:13', '2021-09-17 21:23:13', 0, 'Abbott', '2d70d8f148e3eda8d24cb1a926caed53'),
(6, 'Ajith Jacob', 'ajith.jacob@abbott.com', 'Cochin', 'AV', NULL, NULL, '2021-09-17 19:55:17', '2021-09-17 19:55:17', '2021-09-17 21:25:17', 0, 'Abbott', 'fae92e6d98aef81c67c17bb8fe23a491'),
(7, 'Sojo Joy', 'sojo.joy@abbott.com', 'Cochin', 'Abbott Healthcare ', NULL, NULL, '2021-09-17 19:56:19', '2021-09-17 19:56:19', '2021-09-17 21:26:19', 0, 'Abbott', '0ed96281c3c269999faa45f68dec15a1'),
(8, 'Sudheesh Chandran', 'sudheesh.chandran@abbott.com', 'Trivandrum', 'Abbott', NULL, NULL, '2021-09-17 20:01:09', '2021-09-17 20:01:09', '2021-09-17 21:31:09', 0, 'Abbott', 'de4f8b49f92b30eb2a6571c869deb786'),
(9, 'Jibin Thariyan Thomas', 'jibintthomas@gmail.com', 'Kochi', 'LISIE ', NULL, NULL, '2021-09-17 20:01:31', '2021-09-17 20:01:31', '2021-09-17 21:31:31', 0, 'Abbott', '8d16e8edfa063f3d06e898c1d2c32ebd'),
(10, 'Nishanth', 'support@coact.co.in', 'bangal', 'ban', NULL, NULL, '2021-09-17 20:04:25', '2021-09-17 20:04:25', '2021-09-17 21:34:25', 0, 'Abbott', '9c0e3833d61511a7d95f3e1cfbe16b6c'),
(11, 'Samson', 'samsonbabu50@gmail.com', 'Thrissur', 'Jubille', NULL, NULL, '2021-09-17 20:05:16', '2021-09-17 20:05:16', '2021-09-17 21:35:16', 0, 'Abbott', '6ba3228b0a963776047df2c082983a0d'),
(12, 'Sreethu', 'sreeinhimmel@gmail.com', 'Trivandrum', 'Jubilee Hospital', NULL, NULL, '2021-09-17 20:05:52', '2021-09-17 20:05:52', '2021-09-17 21:35:52', 0, 'Abbott', '71bd88ed2b85da68947d39d6b051247c'),
(13, 'Lijo', 'lijo.k@abbott.com', 'Cochin', 'Abbott', NULL, NULL, '2021-09-17 20:05:55', '2021-09-17 20:05:55', '2021-09-17 21:35:55', 0, 'Abbott', '56381a5c9c1db10ebd7c35402772bacf'),
(14, 'Sakkeer Husain', 'shaheer310@gmail.com', 'Panoor', 'V s m', NULL, NULL, '2021-09-17 20:06:25', '2021-09-17 20:06:25', '2021-09-17 21:36:25', 0, 'Abbott', '1050b1dec2e950fe03df5010f017fbad'),
(15, 'Dr.Vijin', 'vivinjoseph@gmail.com', 'Ernakulam', 'Lisie', NULL, NULL, '2021-09-17 20:08:15', '2021-09-17 20:08:15', '2021-09-17 21:38:15', 0, 'Abbott', '0f50336c41f6653e973c085e68c5c792'),
(16, 'Simboola', 'simboolafathima@gmail.com', 'Trivandrum', 'Mch tvm', NULL, NULL, '2021-09-17 20:09:34', '2021-09-17 20:09:34', '2021-09-17 21:39:34', 0, 'Abbott', '3171bfb4387633a9a2a009118ea54e55'),
(17, 'Akshara ajit', 'aksharaajit@gmail.com', 'Pathanamthitta ', 'Mgm muthoot ', NULL, NULL, '2021-09-17 20:11:38', '2021-09-17 20:11:38', '2021-09-17 21:41:38', 0, 'Abbott', 'fe4b3a3179c21e062abccd660603486c'),
(18, 'Sarath', 'sharathkumartv@yahoo.com', 'Palakkad ', 'District hospital palakkad', NULL, NULL, '2021-09-17 20:11:49', '2021-09-17 20:11:49', '2021-09-17 21:41:49', 0, 'Abbott', '7900a8453299fbc1a76a66374ad92384'),
(19, 'Christy ', 'christy.rajan007@gmail.com', 'Thiruvalla ', 'St gregarious medical mission hospital ', NULL, NULL, '2021-09-17 20:11:53', '2021-09-17 20:11:53', '2021-09-17 21:41:53', 0, 'Abbott', '4c64f6aafba69a39140cc95924f0b591'),
(20, 'Sreethu', 'sreeinhimmel@gmail.com', 'Trivandrum', 'Jubilee Hospital', NULL, NULL, '2021-09-17 20:12:26', '2021-09-17 20:12:26', '2021-09-17 20:30:37', 0, 'Abbott', 'ed95c6d60321ecc9dced70cb802754e0'),
(21, 'SARATH S K', 'sarathsksjm@gmail.com', 'TRIVANDRUM', 'Abbott', NULL, NULL, '2021-09-17 20:13:07', '2021-09-17 20:13:07', '2021-09-17 21:43:07', 0, 'Abbott', '6ed62ef2c406363150029dea458fafc9'),
(22, 'Ullas R Mullamala', 'ullasjc@gmail.com', 'Thodupuzha', 'Holyfamily ', NULL, NULL, '2021-09-17 20:13:13', '2021-09-17 20:13:13', '2021-09-17 21:43:13', 0, 'Abbott', '99091b1fbb9b30deadc569756a93d261'),
(23, 'Sojo Joy', 'sojo.joy@abbott.com', 'Cochin', 'Abbott ', NULL, NULL, '2021-09-17 20:14:23', '2021-09-17 20:14:23', '2021-09-17 21:44:23', 0, 'Abbott', '6025222f14295353824a5209c9b8f2c9'),
(24, 'Samson', 'samsonbabu50@gmail.com', 'Thrissur', 'Jubille', NULL, NULL, '2021-09-17 20:14:36', '2021-09-17 20:14:36', '2021-09-17 21:44:36', 0, 'Abbott', '76b7ca1b5bb5b721711c7f2967aba2f9'),
(25, 'Albin Thomas', 'albin.thomas@asterhospital.com', 'Dubai ', 'Aster hospital ', NULL, NULL, '2021-09-17 20:15:45', '2021-09-17 20:15:45', '2021-09-17 20:27:35', 0, 'Abbott', 'ca4bef3fe47b1d302c8065467994f9d3'),
(26, 'Samson', 'samsonbabu50@gmail.com', 'Thrissur', 'Jubille', NULL, NULL, '2021-09-17 20:16:24', '2021-09-17 20:16:24', '2021-09-17 21:46:24', 0, 'Abbott', '91cd44578c33d494c820f2b0360ca6be'),
(27, 'Ajesh T', 'ajesh.t@abbott.com', 'Calicut', 'Abbott', NULL, NULL, '2021-09-17 20:21:55', '2021-09-17 20:21:55', '2021-09-17 21:51:55', 0, 'Abbott', '5be5878cc90d03e702920dcf9b23ed88'),
(28, 'Joshy', 'joshythomascrc@gmail.com', 'Ernakulam', 'Lisie Hospital', NULL, NULL, '2021-09-17 20:22:07', '2021-09-17 20:22:07', '2021-09-17 21:52:07', 0, 'Abbott', '43b2581b36489a11e0d4593661d2105e'),
(29, 'Mitty George', 'mitz9774@gmail.com', 'trivandrum', 'Mch tvm', NULL, NULL, '2021-09-17 20:22:46', '2021-09-17 20:22:46', '2021-09-17 21:52:46', 0, 'Abbott', '15c028eb562ab05c87c031668ecbff78'),
(30, 'SANDHEEP GEORGE VILLOTH', 'sandheep.villoth@gmail.com', 'THODUPUZHA', 'St Mary\'s Hospital', NULL, NULL, '2021-09-17 20:23:15', '2021-09-17 20:23:15', '2021-09-17 21:53:15', 0, 'Abbott', '182eae454e8b568846bfeb4a25da51b1'),
(31, 'Tahsin', 'drtahsin1@gmail.com', 'Kottakal', 'Aster MIMS', NULL, NULL, '2021-09-17 20:23:29', '2021-09-17 20:23:29', '2021-09-17 21:53:29', 0, 'Abbott', 'c3e68ea1c72c3fc438f916b1e2176e73'),
(32, 'Muneer', 'muneer0487@gmail.com', 'Chavakkad', 'Sevana hospital', NULL, NULL, '2021-09-17 20:23:31', '2021-09-17 20:23:31', '2021-09-17 21:53:31', 0, 'Abbott', '954e06e86b48b14e968b8a23b05d82be'),
(33, 'Cibu Mathew', 'lizcibu@rediffmail.com', 'THRISSUR', 'Mch', NULL, NULL, '2021-09-17 20:28:41', '2021-09-17 20:28:41', '2021-09-17 21:58:41', 0, 'Abbott', '7f57b6d49980c945f7a40700ad9bdf6c'),
(34, 'Cibu Mathew', 'lizcibu@rediffmail.com', 'THRISSUR', 'Mch', NULL, NULL, '2021-09-17 20:28:47', '2021-09-17 20:28:47', '2021-09-17 21:58:47', 0, 'Abbott', 'f5b68bd131754c8dffb5718927a57316'),
(35, 'Cibu Mathew', 'lizcibu@rediffmail.com', 'THRISSUR', 'Mch', NULL, NULL, '2021-09-17 20:28:49', '2021-09-17 20:28:49', '2021-09-17 21:58:49', 0, 'Abbott', '43bbbbb56f5a12c13a23b878fb711f6a'),
(36, 'Cibu Mathew', 'lizcibu@rediffmail.com', 'THRISSUR', 'Mch', NULL, NULL, '2021-09-17 20:28:55', '2021-09-17 20:28:55', '2021-09-17 21:58:55', 0, 'Abbott', '1078aa06b07280dfa772081dd7fd345b'),
(37, 'Dr JO Joseph ', 'drjojoseph@gmail.comm', 'Cochin ', 'Lisie ', NULL, NULL, '2021-09-17 20:31:28', '2021-09-17 20:31:28', '2021-09-17 22:01:28', 0, 'Abbott', 'cbc29617224644f583421c57002a1652'),
(38, 'Albin Thomas', 'albin.thomas@asterhospital.com', 'Dubai', 'Aster hospital ', NULL, NULL, '2021-09-17 20:31:47', '2021-09-17 20:31:47', '2021-09-17 20:52:25', 0, 'Abbott', '65ffab5c6dac9fe128c4e1bbb8037508'),
(39, 'Sheena', 'sheenapjohn93@yahoo.com', 'PHILADELPHIA', 'Jefferson Hospital', NULL, NULL, '2021-09-17 20:31:53', '2021-09-17 20:31:53', '2021-09-17 20:36:54', 0, 'Abbott', 'dcabc6513fe08ba74077c170e56620da'),
(40, 'Santosh Chandran', 'santhosh.chandran@abbott.com', 'Kochi ', 'Abbottt ', NULL, NULL, '2021-09-17 20:33:48', '2021-09-17 20:33:48', '2021-09-17 22:03:48', 0, 'Abbott', '2f47b4d25d03156847b66be449cd24b9'),
(41, 'Bijilesh', 'bijilesh@gmail.com', 'Thrissur', 'Govt medical College Thrissur', NULL, NULL, '2021-09-17 20:34:08', '2021-09-17 20:34:08', '2021-09-17 22:04:08', 0, 'Abbott', 'dc3d6084d6cdcadc10d545520b5f4050'),
(42, 'Sreethu', 'sreeinhimmel@gmail.com', 'Trivandrum', 'Jubilee Hospital', NULL, NULL, '2021-09-17 20:36:03', '2021-09-17 20:36:03', '2021-09-17 20:44:00', 0, 'Abbott', '75a2f07205047da983575e358160ec4d'),
(43, 'Himanshu Rana', 'drhimanshu2005@gmail.com', 'BIJNOR', 'Government Medical college,  Trivandrum ', NULL, NULL, '2021-09-17 20:37:27', '2021-09-17 20:37:27', '2021-09-17 22:07:27', 0, 'Abbott', 'f5b75f276fd64448ff797d587efdc86c'),
(44, 'ABHILASH V P', 'kannanabhi143@gmail.com', 'Thiruvananthapuram', 'GG HOSPITAL ', NULL, NULL, '2021-09-17 20:37:33', '2021-09-17 20:37:33', '2021-09-17 22:07:33', 0, 'Abbott', '32936f2279869b6a16c978d0ab00c0cc'),
(45, 'Naveed', 'doctnaveed@gmail.com', 'Uae', 'Aster', NULL, NULL, '2021-09-17 20:37:48', '2021-09-17 20:37:48', '2021-09-17 22:07:48', 0, 'Abbott', '037d06b194829fa99a7f9f0ef66927f6'),
(46, 'cibu', 'lizcibu@rediffmail.com', 'thrissur', 'mch', NULL, NULL, '2021-09-17 20:37:55', '2021-09-17 20:37:55', '2021-09-17 22:07:55', 0, 'Abbott', 'a7cd9c043a91675f5e4862924859c128'),
(47, 'ramesh nair', 'ramutheboss@gmail.com', 'Thank ', 'Perinthalmanna ', NULL, NULL, '2021-09-17 20:39:38', '2021-09-17 20:39:38', '2021-09-17 22:09:38', 0, 'Abbott', 'a19c8947b38973df6157514888a4cf7b'),
(48, 'SHYAM SASIDHARAN', 'shyamsasi04@gmail.com', 'Kottarakkara ', 'Vijaya', NULL, NULL, '2021-09-17 20:42:26', '2021-09-17 20:42:26', '2021-09-17 22:12:26', 0, 'Abbott', 'aa204c0acb21af5dd4befb7dcdae08b6'),
(49, 'AFSAL', 'afsalpa1992@gmail.com', 'Calicut', 'Aster mims calicut', NULL, NULL, '2021-09-17 20:44:21', '2021-09-17 20:44:21', '2021-09-17 20:52:22', 0, 'Abbott', '714a2f1ab6a0a84f5ae4959008fff10a'),
(50, 'GIREESH Gireesh gopinath', 'gireeshgopinath09@gmail.com', 'Perinthalmanna', 'ems hospital ', NULL, NULL, '2021-09-17 20:44:35', '2021-09-17 20:44:35', '2021-09-17 22:14:35', 0, 'Abbott', 'e823a5a94810cb5c6884f0938fb0c684'),
(51, 'BINJO J VAZHAPPILLY', 'binjovazhappilly@gmail.com', 'ANGAMALY', 'APOLLOADLUX', NULL, NULL, '2021-09-17 20:46:54', '2021-09-17 20:46:54', '2021-09-17 22:16:54', 0, 'Abbott', 'd0364a0ad2e125fa35c962cacadd1d16'),
(52, 'Krishnapriya', 'vishnupriyakrishnapriya@gmail.com', 'Thiruvananthapuram', 'Govt.medical collage', NULL, NULL, '2021-09-17 20:48:02', '2021-09-17 20:48:02', '2021-09-17 22:18:02', 0, 'Abbott', '013c8a0dc5141afab3701a33e126c32b'),
(53, 'Asif', 'asifplavara111@gmail.com', 'Riyadh', 'Ksmc', NULL, NULL, '2021-09-17 20:49:08', '2021-09-17 20:49:08', '2021-09-17 21:23:58', 0, 'Abbott', 'cdd15492a6ae34bc675a0a136c8105d0'),
(54, 'Asif', 'asifplavara111@gmail.com', 'Riyadh', 'Ksmc', NULL, NULL, '2021-09-17 20:49:10', '2021-09-17 20:49:10', '2021-09-17 22:19:10', 0, 'Abbott', '536607bc363ae8b15f0320aa9a5215d8'),
(55, 'Albin Thomas', 'albin.thomas@asterhospital.com', 'Dubai ', 'Aster', NULL, NULL, '2021-09-17 20:52:40', '2021-09-17 20:52:40', '2021-09-17 21:16:39', 0, 'Abbott', '2a7fa7f383aeb0da19a7d02a700a5486'),
(56, 'GIREESH Gireesh gopinath', 'gireeshgopinath09@gmail.com', 'Perinthalmanna', 'ems hospital ', NULL, NULL, '2021-09-17 20:55:43', '2021-09-17 20:55:43', '2021-09-17 22:25:43', 0, 'Abbott', '81718f70cd746feaca61790c18b47cc6'),
(57, 'Aji johnson', 'ajiuricodu@gmail.com', 'Kollam ', 'Meditrina ', NULL, NULL, '2021-09-17 20:57:12', '2021-09-17 20:57:12', '2021-09-17 22:27:12', 0, 'Abbott', 'f0ad5fae25e7131ac205437514572d9f'),
(58, 'Sreethu', 'sreeinhimmel@gmail.com', 'Trivandrum', 'Jubilee Hospital', NULL, NULL, '2021-09-17 21:00:10', '2021-09-17 21:00:10', '2021-09-17 21:12:53', 0, 'Abbott', '225defa93e1846d8601578af1a00bf3d'),
(59, 'Safvan Panakkal', 'safvanboss88@gmail.com', 'Chenni', 'Hh', NULL, NULL, '2021-09-17 21:06:49', '2021-09-17 21:06:49', '2021-09-17 22:36:49', 0, 'Abbott', '8dd9da0506153e2f3fc824bda2013238'),
(60, 'Sreethu', 'sreeinhimmel@gmail.com', 'Trivandrum', 'Jubilee Hospital', NULL, NULL, '2021-09-17 21:26:22', '2021-09-17 21:26:22', '2021-09-17 21:26:46', 0, 'Abbott', 'edfc7537ca7f6817f57dc3ff6fc6dcee'),
(61, 'jose m', 'josemariafron2@gmail.com', 'Punalur', 'Jrh Mumbai central', NULL, NULL, '2021-09-17 21:29:17', '2021-09-17 21:29:17', '2021-09-17 22:59:17', 0, 'Abbott', '48a923366505a091a5989eccad13a3d0'),
(62, 'Ullas R Mullamala', 'ullasjc@gmail.com', 'Thodupuzha', 'Holyfamily ', NULL, NULL, '2021-09-17 21:40:18', '2021-09-17 21:40:18', '2021-09-17 23:10:18', 0, 'Abbott', 'ee86ff0fc8ebde4e6dcae0473b47e2e6'),
(63, 'Nishanth', 'support@coact.co.in', 'bangal', 'ban', NULL, NULL, '2021-09-17 22:22:37', '2021-09-17 22:22:37', '2021-09-17 22:22:46', 0, 'Abbott', '32e4c5647940d44aceb2835568b68f6b'),
(64, 'Nishanth', 'support@coact.co.in', 'bangal', 'ban', NULL, NULL, '2021-09-17 22:22:55', '2021-09-17 22:22:55', '2021-09-17 23:52:55', 0, 'Abbott', 'f8353999562669f17ecaa05ad40ba688'),
(65, 'Ramitha Thyaghese', 'ramitha.thyaghese08@gmail.com', 'Ernakulam', 'Amrita institute of medical sciences', NULL, NULL, '2021-09-28 13:06:01', '2021-09-28 13:06:01', '2021-09-28 14:36:01', 0, 'Abbott', 'd10d52da101ca94c7a4968a461babd1b');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `faculty`
--
ALTER TABLE `faculty`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `faculty_new`
--
ALTER TABLE `faculty_new`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_questions`
--
ALTER TABLE `tbl_questions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_user`
--
ALTER TABLE `tbl_user`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_users`
--
ALTER TABLE `tbl_users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `faculty`
--
ALTER TABLE `faculty`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `faculty_new`
--
ALTER TABLE `faculty_new`
  MODIFY `id` int(50) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_questions`
--
ALTER TABLE `tbl_questions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `tbl_user`
--
ALTER TABLE `tbl_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_users`
--
ALTER TABLE `tbl_users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=66;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
