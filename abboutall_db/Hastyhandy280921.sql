-- phpMyAdmin SQL Dump
-- version 4.6.6deb5ubuntu0.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Jan 14, 2022 at 08:53 AM
-- Server version: 5.7.36-0ubuntu0.18.04.1
-- PHP Version: 7.2.24-0ubuntu0.18.04.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `Hastyhandy280921`
--

-- --------------------------------------------------------

--
-- Table structure for table `faculty`
--

CREATE TABLE `faculty` (
  `id` int(11) NOT NULL,
  `user_name` varchar(200) NOT NULL,
  `user_email` varchar(200) NOT NULL,
  `city` varchar(100) NOT NULL,
  `hospital` varchar(100) NOT NULL,
  `speciality` varchar(100) DEFAULT NULL,
  `reg_no` varchar(100) DEFAULT NULL,
  `joining_date` varchar(100) NOT NULL,
  `login_date` date DEFAULT NULL,
  `logout_date` date DEFAULT NULL,
  `logout_status` int(11) NOT NULL DEFAULT '0' COMMENT '1 = logged in',
  `eventname` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `faculty`
--

INSERT INTO `faculty` (`id`, `user_name`, `user_email`, `city`, `hospital`, `speciality`, `reg_no`, `joining_date`, `login_date`, `logout_date`, `logout_status`, `eventname`) VALUES
(1, 'Nishanth', 'support@coact.co.in', 'bangal', 'ban', NULL, NULL, '2021/09/27 14:22:23', '2021-09-27', '2021-09-27', 1, 'Abbott'),
(2, 'Sudheesh Chandran', 'sudheesh.chandran@abbott.com', 'Triavndrum', 'Abbott', NULL, NULL, '2021/09/28 07:19:59', '2021-09-28', '2021-09-28', 1, 'Abbott'),
(3, 'Nishanth', 'nishanth@coact.co.in', 'bangal', 'ban', NULL, NULL, '2021/09/28 14:55:35', '2021-09-28', '2021-09-28', 1, 'Abbott'),
(4, 'Nishanth', 'nishanth@coact.co.in', 'bangal', 'ban', NULL, NULL, '2021/09/28 18:18:10', '2021-09-28', '2021-09-28', 1, 'Abbott'),
(5, 'praveen gk', 'drgkpraveen@gmail.com', 'Thiruvananthapuram', 'SUT hospital', NULL, NULL, '2021/09/28 18:41:30', '2021-09-28', '2021-09-28', 1, 'Abbott'),
(6, 'sudheesh chandran', 'sudheesh.chandran@abbott.com', 'trivandrum', 'abbott', NULL, NULL, '2021/09/28 18:45:38', '2021-09-28', '2021-09-28', 1, 'Abbott'),
(7, 'Ajesh T', 'ajesh.t@abbott.com', 'Calicut', 'Abbott', NULL, NULL, '2021/09/28 18:54:08', '2021-09-28', '2021-09-28', 1, 'Abbott'),
(8, 'Dr Anurag Passi', 'anuragpassi4@gmail.com', 'Gurgaon', 'Artemis Hospital', NULL, NULL, '2021/09/28 18:55:01', '2021-09-28', '2021-09-28', 1, 'Abbott'),
(9, 'UMESAN C V', 'umesancv@gmail.com', 'kannur', 'Aster MIMS Hospital', NULL, NULL, '2021/09/28 18:55:51', '2021-09-28', '2021-09-28', 1, 'Abbott'),
(10, 'UMESAN C V', 'umesancv@gmail.com', 'kannur', 'Aster MIMS Hospital', NULL, NULL, '2021/09/28 18:56:34', '2021-09-28', '2021-09-28', 1, 'Abbott'),
(11, 'Ajith Jacob', 'ajith.jacob@abbott.com', 'Cochin', 'AV', NULL, NULL, '2021/09/28 18:57:30', '2021-09-28', '2021-09-28', 1, 'Abbott'),
(12, 'UMESAN C V', 'umesancv@gmail.com', 'KANNUR', 'ASTER MIMS, KANNUR', NULL, NULL, '2021/09/28 18:58:59', '2021-09-28', '2021-09-28', 1, 'Abbott'),
(13, 'Dr Girish G', 'girishg_dr@yahoo.com', 'calicut', 'chest hospital', NULL, NULL, '2021/09/28 18:59:31', '2021-09-28', '2021-09-28', 1, 'Abbott'),
(14, 'dr praveen s v', 'drpraveencardio@gmail.com', 'trivandrum', 'kims', NULL, NULL, '2021/09/28 18:59:47', '2021-09-28', '2021-09-28', 1, 'Abbott'),
(15, 'Dr Anurag Passi', 'anuragpassi4@gmail.com', 'Gurgaon', 'Artemis hospital', NULL, NULL, '2021/09/28 19:00:32', '2021-09-28', '2021-09-28', 1, 'Abbott'),
(16, 'Dr PRAVEEN S V', 'drpraveencardio@gmail.com', 'Trivandrum', 'KIMS', NULL, NULL, '2021/09/28 19:00:54', '2021-09-28', '2021-09-28', 1, 'Abbott'),
(17, 'Umesan c v', 'umesancv@gmail.com', 'Kannur, Kerala', 'Aster MIMS', NULL, NULL, '2021/09/28 19:02:12', '2021-09-28', '2021-09-28', 1, 'Abbott'),
(18, 'Ramesh Natarajan', 'natarajanramesh79@gmail.com', 'Trivandrum', 'Kims', NULL, NULL, '2021/09/28 19:02:37', '2021-09-28', '2021-09-28', 1, 'Abbott'),
(19, 'UMESAN C V', 'umesancv@gmail.com', 'KANNUR', 'ASTER MIMS, KANNUR', NULL, NULL, '2021/09/28 19:03:38', '2021-09-28', '2021-09-28', 1, 'Abbott'),
(20, 'Ajesh T', 'ajesh.t@abbott.com', 'Calicut ', 'Abbott', NULL, NULL, '2021/09/28 19:07:43', '2021-09-28', '2021-09-28', 1, 'Abbott'),
(21, 'RAMESH NATARAJAN', 'natarajanramesh79@gmail.com', 'Thiruvananthapuram', 'Kims', NULL, NULL, '2021/09/28 19:16:04', '2021-09-28', '2021-09-28', 1, 'Abbott'),
(22, 'Dr Anurag Passi', 'anuragpassi4@gmail.com', 'Gurgaon', 'Artemis hospital', NULL, NULL, '2021/09/28 19:32:22', '2021-09-28', '2021-09-28', 1, 'Abbott'),
(23, 'Dr Anurag Passi', 'anuragpassi4@gmail.com', 'Gurgaon', 'Artemis hospital', NULL, NULL, '2021/09/28 19:43:54', '2021-09-28', '2021-09-28', 1, 'Abbott'),
(24, 'nischal dubey', 'nischal.dubey@abbott.com', 'Delhi', 'Abbott', NULL, NULL, '2021/09/28 19:49:14', '2021-09-28', '2021-09-28', 1, 'Abbott'),
(25, 'Sudheesh Chandran', 'sudheesh.chandran@abbott.com', 'Trivandrum', 'Abbott', NULL, NULL, '2021/09/28 20:34:57', '2021-09-28', '2021-09-28', 1, 'Abbott');

-- --------------------------------------------------------

--
-- Table structure for table `faculty_new`
--

CREATE TABLE `faculty_new` (
  `id` int(50) NOT NULL,
  `Name` varchar(200) NOT NULL,
  `EmailID` varchar(200) NOT NULL,
  `City` varchar(200) NOT NULL,
  `Hospital` varchar(200) NOT NULL,
  `Last Login On` varchar(200) NOT NULL,
  `Logout Times` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_questions`
--

CREATE TABLE `tbl_questions` (
  `id` int(11) NOT NULL,
  `user_name` varchar(255) NOT NULL,
  `user_email` varchar(255) NOT NULL,
  `user_question` varchar(500) NOT NULL,
  `asked_at` datetime NOT NULL,
  `eventname` varchar(255) NOT NULL,
  `speaker` int(11) NOT NULL DEFAULT '0',
  `answered` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_questions`
--

INSERT INTO `tbl_questions` (`id`, `user_name`, `user_email`, `user_question`, `asked_at`, `eventname`, `speaker`, `answered`) VALUES
(1, 'RONY THOMAS JOHN', 'ronythomasjohn@gmail.com', 'Video is not playing', '2021-09-28 19:07:32', 'Abbott', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_user`
--

CREATE TABLE `tbl_user` (
  `id` int(11) NOT NULL,
  `user_name` varchar(255) NOT NULL,
  `user_email` varchar(500) NOT NULL,
  `city` varchar(100) NOT NULL,
  `hospital` varchar(100) NOT NULL,
  `speciality` varchar(100) DEFAULT NULL,
  `reg_no` varchar(50) DEFAULT NULL,
  `joining_date` datetime NOT NULL,
  `login_date` datetime DEFAULT NULL,
  `logout_date` datetime DEFAULT NULL,
  `logout_status` int(11) NOT NULL DEFAULT '0' COMMENT '1=loggedin',
  `eventname` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_users`
--

CREATE TABLE `tbl_users` (
  `id` int(11) NOT NULL,
  `user_name` varchar(255) NOT NULL,
  `user_email` varchar(500) NOT NULL,
  `city` varchar(100) NOT NULL,
  `hospital` varchar(100) NOT NULL,
  `speciality` varchar(100) DEFAULT NULL,
  `reg_no` varchar(50) DEFAULT NULL,
  `joining_date` datetime NOT NULL,
  `login_date` datetime DEFAULT NULL,
  `logout_date` datetime DEFAULT NULL,
  `logout_status` int(11) NOT NULL DEFAULT '0' COMMENT '1=loggedin',
  `eventname` varchar(255) NOT NULL,
  `token` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_users`
--

INSERT INTO `tbl_users` (`id`, `user_name`, `user_email`, `city`, `hospital`, `speciality`, `reg_no`, `joining_date`, `login_date`, `logout_date`, `logout_status`, `eventname`, `token`) VALUES
(1, 'Nishanth', 'support@coact.co.in', 'bangal', 'ban', NULL, NULL, '2021-09-27 17:30:00', '2021-09-27 17:30:00', '2021-09-27 17:31:07', 0, 'Abbott', '83184fa6e1618c3814da17cc8db1ac1d'),
(2, 'Vibhav ', 'vibhav.abhyankar@gmail.com', 'Pune', 'fwsde', NULL, NULL, '2021-09-27 17:37:14', '2021-09-27 17:37:14', '2021-09-27 17:45:59', 0, 'Abbott', '959b831c022c30ca4d4266224332147e'),
(3, 'Nishanth', 'nishanth@coact.co.in', 'bangal', 'ban', NULL, NULL, '2021-09-27 17:50:27', '2021-09-27 17:50:27', '2021-09-27 17:50:34', 0, 'Abbott', '57eb7f482bbf1176556c850e1a00d25a'),
(4, 'Nishanth', 'nishanth@coact.co.in', 'bangal', 'ban', NULL, NULL, '2021-09-27 17:50:45', '2021-09-27 17:50:45', '2021-09-27 17:51:37', 0, 'Abbott', '8459074e3839ca2f9c6c9aefc86a0b57'),
(5, 'Vibhav ', 'vibhav.abhyankar@gmail.com', 'Pune', 'ABC', NULL, NULL, '2021-09-27 17:52:13', '2021-09-27 17:52:13', '2021-09-27 18:01:38', 0, 'Abbott', 'fc47de3ed4f496967b4c48824e5999a7'),
(6, 'Nishanth', 'support@coact.co.in', 'bangal', 'ban', NULL, NULL, '2021-09-27 17:57:36', '2021-09-27 17:57:36', '2021-09-27 17:59:36', 0, 'Abbott', 'e7c2f9d4752aed6c4239ed4136171beb'),
(7, 'Nishanth', 'nishanth@coact.co.in', 'bangal', 'ban', NULL, NULL, '2021-09-27 17:59:45', '2021-09-27 17:59:45', '2021-09-27 18:00:23', 0, 'Abbott', '5ef3cd6eaef42e2c54991f0b83270931'),
(8, 'Sudheesh Chandran', 'sudheesh.chandran@abbott.com', 'Triavndrum', 'Abbott', NULL, NULL, '2021-09-28 07:32:11', '2021-09-28 07:32:11', '2021-09-28 09:02:11', 0, 'Abbott', '64a0a42f6f1213fd8b76067a8e8a654b'),
(9, 'Saikiran Kakarla', 'doctorsaikirankakarla@gmail.com', 'Trivandrum', 'SCTIMST', NULL, NULL, '2021-09-28 14:18:36', '2021-09-28 14:18:36', '2021-09-28 15:48:36', 0, 'Abbott', '6ff1f092a15d05187275c3e81e06ecfd'),
(10, 'Nishanth', 'nishanth@coact.co.in', 'bangal', 'ban', NULL, NULL, '2021-09-28 18:02:03', '2021-09-28 18:02:03', '2021-09-28 19:32:03', 1, 'Abbott', '1a7e2cc1aaf381378699c54faf23f817'),
(11, 'Nishanth', 'nishanth@coact.co.in', 'bangal', 'ban', NULL, NULL, '2021-09-28 18:02:03', '2021-09-28 18:02:03', '2021-09-28 19:32:03', 1, 'Abbott', '8e54a05c08a34e77e9b4dab2e2626fc4'),
(12, 'Meenu', 'lethikameenu@gmail.com', 'KOLLAM', 'SUT', NULL, NULL, '2021-09-28 18:29:00', '2021-09-28 18:29:00', '2021-09-28 19:59:00', 1, 'Abbott', '4783e68d9a16b3344fb6f1a3a80269f1'),
(13, 'Meenu', 'lethikameenu@gmail.com', 'KOLLAM', 'SUT', NULL, NULL, '2021-09-28 18:29:16', '2021-09-28 18:29:16', '2021-09-28 19:59:16', 1, 'Abbott', '0ac2b62c3275ec8d2f1687c9783286a4'),
(14, 'Sakkeer Husain', 'shaheer310@gmail.com', 'Panoor', 'V s m', NULL, NULL, '2021-09-28 18:29:22', '2021-09-28 18:29:22', '2021-09-28 19:59:22', 1, 'Abbott', '97e33b39a2bd3598d332fa048cd58413'),
(15, 'PARVATHY J NAIR', 'parvathymangalathu@gmail.com', 'TRIVANDRUM', 'SUT HOSPITAL,PATTOM', NULL, NULL, '2021-09-28 18:32:33', '2021-09-28 18:32:33', '2021-09-28 20:02:33', 1, 'Abbott', '52c70ab2733a0df5e7a5e9931b9dcce8'),
(16, 'PARVATHY', 'parvathymangalathu@gmail.com', 'TRIVANDRUM', 'SUT HOSPITAL,PATTOM', NULL, NULL, '2021-09-28 18:58:34', '2021-09-28 18:58:34', '2021-09-28 20:28:34', 1, 'Abbott', '7d4b84e59a24cf0e91e77cfff3df7f84'),
(17, 'RONY THOMAS JOHN', 'ronythomasjohn@gmail.com', 'Parumala', 'St. Gregorious mission hospital ', NULL, NULL, '2021-09-28 18:58:35', '2021-09-28 18:58:35', '2021-09-28 20:28:35', 1, 'Abbott', '721d11ba0ac6dd317a2944b9dbed59cc'),
(18, 'RONY THOMAS JOHN', 'ronythomasjohn@gmail.com', 'Parumala', 'St. Gregorious mission hospital ', NULL, NULL, '2021-09-28 18:58:55', '2021-09-28 18:58:55', '2021-09-28 20:28:55', 1, 'Abbott', 'bccd64b3dc2a8e74cee1ff3cf83af96e'),
(19, 'Krishnapriya', 'vishnupriyakrishnapriya@gmail.com', 'Thiruvananthapuram', 'Govt.medical collage', NULL, NULL, '2021-09-28 19:00:24', '2021-09-28 19:00:24', '2021-09-28 20:30:24', 1, 'Abbott', '90535ab76f3eb261d41059f29179b212'),
(20, 'Krishnapriya', 'vishnupriyakrishnapriya@gmail.com', 'Thiruvananthapuram', 'Govt.medical collage', NULL, NULL, '2021-09-28 19:00:47', '2021-09-28 19:00:47', '2021-09-28 20:30:47', 1, 'Abbott', '01e53d1410cb1bb72c0401c2cbd81ab9'),
(21, 'VINU', 'vinu.raj4747@gmail.com', 'Trivandrum', 'TMC', NULL, NULL, '2021-09-28 19:00:55', '2021-09-28 19:00:55', '2021-09-28 20:30:55', 1, 'Abbott', '85296cc246912c82705ca186b06e5a3b'),
(22, 'Sujesh V S', 'sujesh.vs@abbott.com', 'Cochin ', 'Abbott Vascular', NULL, NULL, '2021-09-28 19:01:02', '2021-09-28 19:01:02', '2021-09-28 20:31:02', 1, 'Abbott', 'd5ba8fc566d3670e96d20281f3ef5398'),
(23, 'RONY THOMAS JOHN', 'ronythomasjohn@gmail.com', 'Parumala', 'St. Gregorious mission hospital ', NULL, NULL, '2021-09-28 19:01:20', '2021-09-28 19:01:20', '2021-09-28 20:31:20', 1, 'Abbott', '148740a77c531b679c2aee40a7e77362'),
(24, 'Sujesh V S', 'sujesh.vs@abbott.com', 'Cochin ', 'Abbott Vascular', NULL, NULL, '2021-09-28 19:01:25', '2021-09-28 19:01:25', '2021-09-28 20:31:25', 1, 'Abbott', '739fa481fb6f2150727be4f40eaa1d60'),
(25, 'Krishnapriya', 'vishnupriyakrishnapriya@gmail.com', 'Thiruvananthapuram', 'Govt.medical collage', NULL, NULL, '2021-09-28 19:01:30', '2021-09-28 19:01:30', '2021-09-28 20:31:30', 1, 'Abbott', 'f9815e4e840d607d3ec406b1b2921aad'),
(26, 'Nishanth', 'nishanth@coact.co.in', 'bangal', 'ban', NULL, NULL, '2021-09-28 19:02:38', '2021-09-28 19:02:38', '2021-09-28 19:07:59', 0, 'Abbott', '0f0560e0f2a783c17c9212fc7711b9b8'),
(27, 'Krishnapriya', 'vishnupriyakrishnapriya@gmail.com', 'Thiruvananthapuram', 'Govt.medical collage', NULL, NULL, '2021-09-28 19:02:44', '2021-09-28 19:02:44', '2021-09-28 20:32:44', 1, 'Abbott', 'e424120b0549827e4aef3a1c34faab9c'),
(28, 'Varun', 'varun.v1113@gmail.com', 'Tvm', 'Mch', NULL, NULL, '2021-09-28 19:03:27', '2021-09-28 19:03:27', '2021-09-28 20:33:27', 1, 'Abbott', 'e48011096da263789821be2f9241c9c0'),
(29, 'Sindhu', 'sindhu.s@kimsglobal.com', 'Trivandrum', 'Kims', NULL, NULL, '2021-09-28 19:03:29', '2021-09-28 19:03:29', '2021-09-28 20:33:29', 1, 'Abbott', '075f2f6efa161e53b50146ae8f9a7a92'),
(30, 'Sindhu', 'sindhu.s@kimsglobal.com', 'Trivandrum', 'Kims', NULL, NULL, '2021-09-28 19:03:50', '2021-09-28 19:03:50', '2021-09-28 20:33:50', 1, 'Abbott', 'cbd8012a062554398ad28492d1570208'),
(31, 'Sindhu', 'sindhu.s@kimsglobal.com', 'Trivandrum', 'Kims', NULL, NULL, '2021-09-28 19:04:44', '2021-09-28 19:04:44', '2021-09-28 20:34:44', 1, 'Abbott', '11d79c195bacdb709d24441d6c43410e'),
(32, 'Sindhu', 'sindhu.s@kimsglobal.com', 'Trivandrum', 'Kims', NULL, NULL, '2021-09-28 19:06:35', '2021-09-28 19:06:35', '2021-09-28 20:36:35', 1, 'Abbott', '10fbc85c708b649ee7c00373b404ab7d'),
(33, 'RONY THOMAS JOHN', 'ronythomasjohn@gmail.com', 'Parumala', 'St. Gregorious mission hospital ', NULL, NULL, '2021-09-28 19:06:43', '2021-09-28 19:06:43', '2021-09-28 20:36:43', 1, 'Abbott', 'd10ddd0385ba200a7c067c4cbcfca52f'),
(34, 'Simboola', 'simboolafathima@gmail.com', 'Trivandrum', 'Mch tvm', NULL, NULL, '2021-09-28 19:06:45', '2021-09-28 19:06:45', '2021-09-28 20:36:45', 1, 'Abbott', '6c43c17d4d8d4829589745ec601f92dc'),
(35, 'Simboola', 'simboolafathima@gmail.com', 'Trivandrum', 'Mch tvm', NULL, NULL, '2021-09-28 19:06:47', '2021-09-28 19:06:47', '2021-09-28 20:36:47', 1, 'Abbott', 'a756096672da82c352ae26c0e5b28172'),
(36, 'Ajesh T', 'ajesh.t@abbott.com', 'Calicut', 'Abbott', NULL, NULL, '2021-09-28 19:06:54', '2021-09-28 19:06:54', '2021-09-28 20:36:54', 1, 'Abbott', '59f7325f5be99b06458f7065b664f772'),
(37, 'muneer', 'muneer0487@gmail.com', 'Pattambi', 'Sevana hospital', NULL, NULL, '2021-09-28 19:08:16', '2021-09-28 19:08:16', '2021-09-28 20:38:16', 1, 'Abbott', '0b4389a762d54ac4fffd01afc2370da6'),
(38, 'Manu Bhasker', 'manuyuvaveer@icloud.com', 'Palakkad ', 'Thangam hospital ', NULL, NULL, '2021-09-28 19:08:41', '2021-09-28 19:08:41', '2021-09-28 20:38:41', 1, 'Abbott', '3c813e851dd9a7285c870b68c17961ee'),
(39, 'Sindhu', 'sindhu.s@kimsglobal.com', 'Trivandrum', 'Kims', NULL, NULL, '2021-09-28 19:08:56', '2021-09-28 19:08:56', '2021-09-28 20:38:56', 1, 'Abbott', 'c4667c18f272c4a20b125dc786ddad86'),
(40, 'RONY THOMAS JOHN', 'ronythomasjohn@gmail.com', 'Parumala', 'St. Gregorious mission hospital ', NULL, NULL, '2021-09-28 19:09:04', '2021-09-28 19:09:04', '2021-09-28 19:11:16', 0, 'Abbott', '2d2856517c248e5e2d15d0c8ec36fb2b'),
(41, 'Nishanth', 'nishanth@coact.co.in', 'bangal', 'ban', NULL, NULL, '2021-09-28 19:09:09', '2021-09-28 19:09:09', '2021-09-28 19:09:16', 0, 'Abbott', '7052fbaf54175c5e60cb86c13b979e75'),
(42, 'Ajesh T', 'ajesh.t@abbott.com', 'Calicut', 'Abbott', NULL, NULL, '2021-09-28 19:09:30', '2021-09-28 19:09:30', '2021-09-28 20:39:30', 1, 'Abbott', 'ea20faee1416db1d3372b42d095e9154'),
(43, 'Sona Lekshmi M S', 'sonalekshmi425@gmail.com', 'Kollam', 'Govt medical college trivandrum', NULL, NULL, '2021-09-28 19:11:44', '2021-09-28 19:11:44', '2021-09-28 20:41:44', 1, 'Abbott', '66af1dc6d9b9ec2bd3f11a8255e621f1'),
(44, 'Krishnapriya', 'vishnupriyakrishnapriya@gmail.com', 'Thiruvananthapuram', 'Govt.medical collage', NULL, NULL, '2021-09-28 19:11:53', '2021-09-28 19:11:53', '2021-09-28 20:41:53', 1, 'Abbott', '63af7aa45a18e3556605236200f297bd'),
(45, 'Nishanth S', 'nishu_8989@yahoo.com', 'Bangalore', 'Bangalore', NULL, NULL, '2021-09-28 19:13:12', '2021-09-28 19:13:12', '2021-09-28 19:13:22', 0, 'Abbott', 'a1adef295c7c87058ad73e8a21cc72df'),
(46, 'Sakkeer Husain', 'shaheer310@gmail.com', 'Panoor', 'V s m', NULL, NULL, '2021-09-28 19:13:16', '2021-09-28 19:13:16', '2021-09-28 20:43:16', 1, 'Abbott', '995f5325c8859bc7e0b98ec2dc7f740e'),
(47, 'JITHIN K', 'Jithinmohan114@gmail.com', 'Kannur', 'Aster MIMS Kannur', NULL, NULL, '2021-09-28 19:14:00', '2021-09-28 19:14:00', '2021-09-28 20:44:00', 1, 'Abbott', '77cec1ee45d259dae6fc6fdd8da25e67'),
(48, 'Navajith V T', 'navajith.thankam@abbott.con', 'Calicut ', 'AV', NULL, NULL, '2021-09-28 19:14:32', '2021-09-28 19:14:32', '2021-09-28 20:44:32', 1, 'Abbott', 'e4e1a7789968ec7b8d037c86822e8ea6'),
(49, 'Akhil Krishna R', 'akhilkrishna282@gmail.com', 'Dubai', 'zulekha hospital', NULL, NULL, '2021-09-28 19:15:21', '2021-09-28 19:15:21', '2021-09-28 20:45:21', 1, 'Abbott', 'cba02ae08122cc981c62e10c3b24959d'),
(50, 'Madhu Sreedharan', 'madhusreedharan@yahoo.com', 'Trivandrum', 'Nims', NULL, NULL, '2021-09-28 19:16:28', '2021-09-28 19:16:28', '2021-09-28 20:46:28', 0, 'Abbott', '854d7d7888dc317b3035fce5c411b1e7'),
(51, 'John', 'johnk@gmail.com', 'Thiruvalla', 'Believers Hospital', NULL, NULL, '2021-09-28 19:16:35', '2021-09-28 19:16:35', '2021-09-28 20:46:35', 0, 'Abbott', '3209ab79653a19340f30de97929275fe'),
(52, 'Dr Suhail M', 'drsuhail@gmail.com', 'Kottakal', 'Aster mims ', NULL, NULL, '2021-09-28 19:17:19', '2021-09-28 19:17:19', '2021-09-28 20:47:19', 0, 'Abbott', 'b316a063bef42baba59c55f7d1049bdb'),
(53, 'Dr. Saji Jose', 'sajijs@yahoo.com', 'Thiruvalla', 'TMM Hospital', NULL, NULL, '2021-09-28 19:17:46', '2021-09-28 19:17:46', '2021-09-28 20:47:46', 0, 'Abbott', '294decf95f30b52029ef028610130322'),
(54, 'Raghu', 'raghu.g.cath@gmail.com', 'Kottiyam', 'Holy cross hospital', NULL, NULL, '2021-09-28 19:18:38', '2021-09-28 19:18:38', '2021-09-28 20:48:38', 0, 'Abbott', '8290c61e136a55b9fd75d363895779b4'),
(55, 'Sojo George Joy', 'sojo.joy@abbott.com', 'Cochin ', 'Abbott Healthcare', NULL, NULL, '2021-09-28 19:20:48', '2021-09-28 19:20:48', '2021-09-28 20:50:48', 0, 'Abbott', '414319f8f6d3248f91d613c167c7d424'),
(56, 'Sajan Ahmad', 'drsajanahamad@gmail.com', 'Thiruvalla', 'St. Gregorios Medical Mission', NULL, NULL, '2021-09-28 19:21:15', '2021-09-28 19:21:15', '2021-09-28 20:51:15', 0, 'Abbott', 'c3a29eb70d5636d1272ebd17d35401a3'),
(57, 'Sujesh V S', 'sujesh.vs@abbott.com', 'Cochin ', 'Abbott', NULL, NULL, '2021-09-28 19:21:48', '2021-09-28 19:21:48', '2021-09-28 20:51:48', 0, 'Abbott', 'b8789b42373b9dcab1952b283877b9a6'),
(58, 'Raghu', 'raghu.g.cath@gmail.com', 'Kottiyam', 'Holy cross hospital', NULL, NULL, '2021-09-28 19:22:46', '2021-09-28 19:22:46', '2021-09-28 20:52:46', 0, 'Abbott', '928f57c48c09b945f9bcad39c7303c3d'),
(59, 'ramesh nair', 'rameshmnair007@gmail.com', 'Calicut ', 'NA', NULL, NULL, '2021-09-28 19:23:38', '2021-09-28 19:23:38', '2021-09-28 20:53:38', 0, 'Abbott', 'e94cd2f4b5d459a66bb157a5c0cebcc9'),
(60, 'SARATH S K', 'sarathsksjm@gmail.com', 'TRIVANDRUM', 'Abbott', NULL, NULL, '2021-09-28 19:23:45', '2021-09-28 19:23:45', '2021-09-28 20:53:45', 0, 'Abbott', 'fbbab6755b8332423d106b587849e5b0'),
(61, 'SANTHOSH CHANDRAN', 'santhosh.chandran@abbott.com', 'Ernakulam', 'Abbott Vascular', NULL, NULL, '2021-09-28 19:24:37', '2021-09-28 19:24:37', '2021-09-28 20:54:37', 0, 'Abbott', '983ade3763ea25e2458481b030f6bed0'),
(62, 'Dr Mathews paul ', 'c@gmail.com', 'Calicut ', 'Moulana ', NULL, NULL, '2021-09-28 19:25:21', '2021-09-28 19:25:21', '2021-09-28 19:25:46', 0, 'Abbott', '8dfbd024637f411f1ecd1b59c004be26'),
(63, 'Dr Ajay', 'draj@gmail.com', 'Calicut ', 'Moulana ', NULL, NULL, '2021-09-28 19:26:20', '2021-09-28 19:26:20', '2021-09-28 20:56:20', 0, 'Abbott', 'a92598f92af8aabf51358322f1cc1596'),
(64, 'Dr Sunil', 'drsp@gmail.com', 'Calicut ', 'EMS', NULL, NULL, '2021-09-28 19:27:47', '2021-09-28 19:27:47', '2021-09-28 20:57:47', 0, 'Abbott', '1000c540d04be26aa52b95d088f76457'),
(65, 'DINU', 'dinu.chndrn@gmail.com', 'THIRUVALLA', 'pushpagiri', NULL, NULL, '2021-09-28 19:28:31', '2021-09-28 19:28:31', '2021-09-28 20:58:31', 0, 'Abbott', '69bcc0e1b086ef665016e6e87e56f1b8'),
(66, 'Dr Anil', 'dranil@gmail.com', 'Calicut ', 'Mims', NULL, NULL, '2021-09-28 19:29:07', '2021-09-28 19:29:07', '2021-09-28 20:59:07', 0, 'Abbott', 'deb9018847c8fcb70794f18843ced844'),
(67, 'Bhavya', 'dinu.chndrn@gmail.com', 'Trivandrum', 'TMM', NULL, NULL, '2021-09-28 19:29:31', '2021-09-28 19:29:31', '2021-09-28 20:59:31', 0, 'Abbott', '192e203143ef81a6a9220f8bcc0dcb79'),
(68, 'Abraham', 'akv9995@gmail.com', 'Kallisery', 'Kmcims', NULL, NULL, '2021-09-28 19:29:49', '2021-09-28 19:29:49', '2021-09-28 19:32:23', 0, 'Abbott', 'ec6ef999ea0f08832c38d8c26cc40a2b'),
(69, 'Neha', 'nehasuresh09@gmail.com', 'Trivandrum', 'Trivandrum medical college', NULL, NULL, '2021-09-28 19:29:49', '2021-09-28 19:29:49', '2021-09-28 20:59:49', 0, 'Abbott', '456f91aad9dff8396c5763e1f608ee79'),
(70, 'Dr Vijayan ', 'drvj@gmail.com', 'Calicut ', 'Nims', NULL, NULL, '2021-09-28 19:30:45', '2021-09-28 19:30:45', '2021-09-28 21:00:45', 0, 'Abbott', '5bab5f9014d40c40476462dbfef94fcb'),
(71, 'Sakkeer Husain', 'shaheer310@gmail.com', 'Panoor', 'V s m', NULL, NULL, '2021-09-28 19:30:56', '2021-09-28 19:30:56', '2021-09-28 21:00:56', 0, 'Abbott', '5fc51e6733cae7ac98836a11ff9c5445'),
(72, 'Sarath Kumar ', 'sharathkumartv@yahoo.com', 'Palakkad ', 'DH palakkad', NULL, NULL, '2021-09-28 19:31:50', '2021-09-28 19:31:50', '2021-09-28 21:01:50', 0, 'Abbott', '40e376de470d36a0db4adf80352e73a2'),
(73, 'Aji johnson', 'ajiuricodu@gmail.com', 'Kollam ', 'Meditrina', NULL, NULL, '2021-09-28 19:33:08', '2021-09-28 19:33:08', '2021-09-28 20:07:33', 0, 'Abbott', '4e2a790fbc1a86b4d0d0a5794410e822'),
(74, 'Dr Manu', 'manudr@gmail.com', 'Dr Manu', 'Thankam', NULL, NULL, '2021-09-28 19:33:41', '2021-09-28 19:33:41', '2021-09-28 21:03:41', 0, 'Abbott', '39a523ab341b0ab11e693681026f0420'),
(75, 'Lijo ', 'lijo.k@abbott.com', 'Cochin ', 'Abbott ', NULL, NULL, '2021-09-28 19:34:04', '2021-09-28 19:34:04', '2021-09-28 21:04:04', 0, 'Abbott', '309029b5733c2e0845b3386ff4349277'),
(76, 'Sujesh V S', 'sujeshvs001@gmail.com', 'Cochin ', 'Abbott', NULL, NULL, '2021-09-28 19:34:15', '2021-09-28 19:34:15', '2021-09-28 21:04:15', 0, 'Abbott', '40893a538c17b3b42924405f475bdd35'),
(77, 'Praveen Nair ', 'praveenaranya@gmail.com', 'Dubai', 'MCH', NULL, NULL, '2021-09-28 19:35:30', '2021-09-28 19:35:30', '2021-09-28 21:05:30', 0, 'Abbott', '4215f0e430eed76fdb3e53d3bac9cc22'),
(78, 'Ajith Jacob', 'ajith.jacob@abbott.com', 'Cochin', 'AV', NULL, NULL, '2021-09-28 19:37:51', '2021-09-28 19:37:51', '2021-09-28 21:07:51', 0, 'Abbott', '21b07e2e971b4887d3fd2271a7b10349'),
(79, 'Abdul Khalam', 'khalamsut@gmail.com', 'Trivandrum', 'Royal hospital', NULL, NULL, '2021-09-28 19:40:20', '2021-09-28 19:40:20', '2021-09-28 21:10:20', 0, 'Abbott', 'f24b7e511df2deb1281c36f7cb8b8087'),
(80, 'Afsal.P.A', 'afsalpa1992@gmail.com', 'Calicut', 'Aster mims calicut', NULL, NULL, '2021-09-28 19:41:57', '2021-09-28 19:41:57', '2021-09-28 21:11:57', 0, 'Abbott', '863eea2062febc58d3e078a8390f85b3'),
(81, 'Ajesh T', 'ajesh.t@abbott.com', 'Calicut', 'Abbott', NULL, NULL, '2021-09-28 19:43:10', '2021-09-28 19:43:10', '2021-09-28 21:13:10', 0, 'Abbott', '00500a472c10a6a63ab4d93b0cd9778c'),
(82, 'Shithin Chacko', 'svarghese71@gmail.com', 'KOLLAM', 'NS MIMS KOLLAM', NULL, NULL, '2021-09-28 19:44:47', '2021-09-28 19:44:47', '2021-09-28 21:14:47', 0, 'Abbott', '4e26dfb13262905cc6ab380839408e6e'),
(83, 'Manu Bhasker', 'manuyuvaveer@icloud.com', 'Palakkad ', 'ThangomHosital ', NULL, NULL, '2021-09-28 19:46:30', '2021-09-28 19:46:30', '2021-09-28 21:16:30', 0, 'Abbott', '201a0c9252b613b3c28722fbfe26298e'),
(84, 'vinod manikandan', 'vinod.manikandan79@gmail.com', 'Kottiyam ', 'Holycross ', NULL, NULL, '2021-09-28 19:46:50', '2021-09-28 19:46:50', '2021-09-28 21:16:50', 0, 'Abbott', 'ef28b3ef674082f17ca37bc2d1f48e4e'),
(85, 'Christy ', 'christy.rajan007@gmail.com', 'Thiruvalla ', 'St gregarious medical mission hospital ', NULL, NULL, '2021-09-28 19:50:17', '2021-09-28 19:50:17', '2021-09-28 19:54:42', 0, 'Abbott', '642fe1472e0e23c61bfe5ff73769b553'),
(86, 'Sakkeer Husain', 'shaheer310@gmail.com', 'Panoor', 'V s m', NULL, NULL, '2021-09-28 19:51:29', '2021-09-28 19:51:29', '2021-09-28 21:21:29', 0, 'Abbott', '93aff95686fbdda30f9d675e59315ee7'),
(87, 'nischal dubey', 'nischal.dubey@abbott.com', 'Delhi', 'Abbott', NULL, NULL, '2021-09-28 19:52:44', '2021-09-28 19:52:44', '2021-09-28 19:54:09', 0, 'Abbott', '62c300c1a59def00adc58c25ed76140b'),
(88, 'nischal dubey', 'nischal.dubey@abbott.com', 'Dehi', 'Abbott', NULL, NULL, '2021-09-28 19:54:32', '2021-09-28 19:54:32', '2021-09-28 21:24:32', 0, 'Abbott', '1e189e3a9d12a337145e7f29a840da46'),
(89, 'Sindhu', 'sindhu.s@kimsglobal.com', 'Trivandrum', 'Kims', NULL, NULL, '2021-09-28 19:56:39', '2021-09-28 19:56:39', '2021-09-28 20:35:16', 0, 'Abbott', 'd140b1ea0ce8eaa75ef09e12c797f166'),
(90, 'Meenu', 'lethikameenu@gmail.com', 'KOLLAM', 'SUT', NULL, NULL, '2021-09-28 20:03:29', '2021-09-28 20:03:29', '2021-09-28 20:03:46', 0, 'Abbott', 'f1499ba376236342483187900f2e743f'),
(91, 'Sujesh V S', 'sujesh.vs@abbott.com', 'Cochin ', 'Abbott ', NULL, NULL, '2021-09-28 20:04:37', '2021-09-28 20:04:37', '2021-09-28 21:34:37', 0, 'Abbott', '376e74b04e35d2b9334a9441108d227d'),
(92, 'Sujesh V S', 'sujesh.vs@abbott.com', 'Cochin ', 'Abbott ', NULL, NULL, '2021-09-28 20:04:38', '2021-09-28 20:04:38', '2021-09-28 21:34:38', 0, 'Abbott', '0e25249084150c8e36fec4ca7de97f24'),
(93, 'Ajesh T', 'ajesh.t@abbott.com', 'Calicut', 'Abbott', NULL, NULL, '2021-09-28 20:10:16', '2021-09-28 20:10:16', '2021-09-28 21:40:16', 0, 'Abbott', 'ae19ab17b330181bfb209539805e2742'),
(94, 'Sujesh V S', 'sujesh.vs@abbott.com', 'Cochin ', 'Abbott', NULL, NULL, '2021-09-28 20:10:41', '2021-09-28 20:10:41', '2021-09-28 21:40:41', 0, 'Abbott', 'd4f821777ad736073045fb209eda95f9'),
(95, 'Safvan Panakkal', 'safvanboss88@gmail.com', 'Chenni', 'Hk', NULL, NULL, '2021-09-28 20:13:57', '2021-09-28 20:13:57', '2021-09-28 21:43:57', 0, 'Abbott', 'a1c8f2b75e920c219638622bc6679225'),
(96, 'Safvan Panakkal', 'safvanboss88@gmail.com', 'Chenni', 'Hk', NULL, NULL, '2021-09-28 20:16:17', '2021-09-28 20:16:17', '2021-09-28 21:46:17', 0, 'Abbott', '30005c17dc6df08cc7ca4dbba9b101d6'),
(97, 'PARVATHY', 'parvathymangalathu@gmail.com', 'trivandrum', 'sut hospital,pattom', NULL, NULL, '2021-09-28 20:16:24', '2021-09-28 20:16:24', '2021-09-28 21:46:24', 0, 'Abbott', '84978c2b5088f2cd9cbb748b32d263bd'),
(98, 'Manu Bhasker', 'manuyuvaveer@icloud.com', 'Palakkad ', 'Thangam hospital ', NULL, NULL, '2021-09-28 20:18:16', '2021-09-28 20:18:16', '2021-09-28 21:48:16', 0, 'Abbott', 'e0d5022bd4c3171ebef452c287d651d9'),
(99, 'Simboola', 'simboolafathima@gmail.com', 'Trivandrum', 'Mch tvm', NULL, NULL, '2021-09-28 20:38:03', '2021-09-28 20:38:03', '2021-09-28 22:08:03', 0, 'Abbott', '44969b22673afda274ba9a67f3fa31ac'),
(100, 'Simboola', 'simboolafathima@gmail.com', 'Trivandrum', 'Mch tvm', NULL, NULL, '2021-09-28 20:38:29', '2021-09-28 20:38:29', '2021-09-28 22:08:29', 0, 'Abbott', 'cb3f376166168f319e9ed516257afeb1'),
(101, 'Simboola', 'simboolafathima@gmail.com', 'Trivandrum', 'Mch tvm', NULL, NULL, '2021-09-28 20:38:55', '2021-09-28 20:38:55', '2021-09-28 20:39:05', 0, 'Abbott', 'be23659432a464f6869f3cf80038c2f2'),
(102, 'BIJU P BENNY', 'mail2biju00@gmail.com', 'BANGLORE', 'NARAYANA HRUDAYALAYA', NULL, NULL, '2021-09-28 20:38:57', '2021-09-28 20:38:57', '2021-09-28 22:08:57', 0, 'Abbott', '47380cfa9e258a5f50f0e3f9c379e851'),
(103, 'simboolafathima@gmail.com', 'simboolafathima@gmail.com', 'Trivandrum', 'Mch tvm', NULL, NULL, '2021-09-28 20:39:24', '2021-09-28 20:39:24', '2021-09-28 22:09:24', 0, 'Abbott', '08ad55b5f10757e6c409ec421039f362'),
(104, 'Raghu', 'raghu.g.cath@gmail.com', 'Kottiyam', 'Holy Cross hospital', NULL, NULL, '2021-09-28 20:40:54', '2021-09-28 20:40:54', '2021-09-28 22:10:54', 0, 'Abbott', '108923db9d546c4a350ea61516ece055'),
(105, 'SARATH S K', 'sarathsksjm@gmail.com', 'TRIVANDRUM', 'Abbott', NULL, NULL, '2021-09-28 20:41:00', '2021-09-28 20:41:00', '2021-09-28 22:11:00', 0, 'Abbott', 'ab72f0c5cae44e09a406f9781481eccf'),
(106, 'Raghu', 'raghu.g.cath@gmail.com', 'Kottiyam', 'Holy Cross hospital', NULL, NULL, '2021-09-28 20:43:59', '2021-09-28 20:43:59', '2021-09-28 22:13:59', 0, 'Abbott', '4919fa837de7758a5164273fbf9861ea'),
(107, 'Akshara ajit', 'aksharaajit@gmail.com', 'Pathanamthitta ', 'Mgm muthoot ', NULL, NULL, '2021-09-29 20:07:30', '2021-09-29 20:07:30', '2021-09-29 21:37:30', 0, 'Abbott', '765b833f3c52bb26050df451cf4c70a2');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `faculty`
--
ALTER TABLE `faculty`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `faculty_new`
--
ALTER TABLE `faculty_new`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_questions`
--
ALTER TABLE `tbl_questions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_user`
--
ALTER TABLE `tbl_user`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_users`
--
ALTER TABLE `tbl_users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `faculty`
--
ALTER TABLE `faculty`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;
--
-- AUTO_INCREMENT for table `faculty_new`
--
ALTER TABLE `faculty_new`
  MODIFY `id` int(50) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_questions`
--
ALTER TABLE `tbl_questions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `tbl_user`
--
ALTER TABLE `tbl_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_users`
--
ALTER TABLE `tbl_users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=108;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
