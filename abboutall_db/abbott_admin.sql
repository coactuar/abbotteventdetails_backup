-- phpMyAdmin SQL Dump
-- version 4.6.6deb5ubuntu0.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Jan 14, 2022 at 06:28 AM
-- Server version: 5.7.36-0ubuntu0.18.04.1
-- PHP Version: 7.2.24-0ubuntu0.18.04.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `abbott_admin`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_users`
--

CREATE TABLE `tbl_users` (
  `id` int(11) NOT NULL,
  `user_name` varchar(255) NOT NULL,
  `user_email` varchar(500) NOT NULL,
  `city` varchar(100) NOT NULL,
  `hospital` varchar(100) NOT NULL,
  `speciality` varchar(100) DEFAULT NULL,
  `userslist` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_users`
--

INSERT INTO `tbl_users` (`id`, `user_name`, `user_email`, `city`, `hospital`, `speciality`, `userslist`) VALUES
(1, 'Completed', 'Webinar on Complex Coronary Intervention', '2021-08-07', '19:00', 'https://drive.google.com/file/d/1qfeXLIYfvHdGqzDlVFhj1EVRpwcisXYW/view?usp=sharing', 'https://drive.google.com/drive/folders/1zCGoxyMmuuGxBTyqm-sr0gs7hHibSbOy?usp=sharing'),
(2, 'Completed', 'The most challenging Primary Angioplasty', '2021-09-17', '09:30', 'https://drive.google.com/file/d/1zuFPeOEo9rdG-CyUx79wK5Be6qARmZgd/view?usp=sharing', 'https://drive.google.com/drive/folders/1_YCvi1Qt3VjQcemhRRtKfznmneOs-7Vq?usp=sharing'),
(3, 'Completed', 'Hasty Handy Case', '2021-09-28', '07:00', 'https://drive.google.com/file/d/1_KM3LJqnbeVo35Md8iKEb7mLjguMPaEZ/view?usp=sharing', 'https://drive.google.com/file/d/1_KM3LJqnbeVo35Md8iKEb7mLjguMPaEZ/view?usp=sharing'),
(4, 'Completed', 'Majic Series 2021 series3', '2021-09-30', '07:00', 'https://drive.google.com/file/d/1yVj96pyH_fA2OKVL92U23psNRrXl-_zW/view?usp=sharing', 'https://drive.google.com/drive/folders/1Oet8VCURSJ69ADgJJf41TcfasKlSQMnd?usp=sharing'),
(5, 'Completed', 'Complexity in PCI', '2021-11-11', '19:00', 'https://drive.google.com/file/d/1Azaq-Jm7WcJbdYolW1__qSuEUJKbcKVj/view?usp=sharing', 'https://drive.google.com/drive/folders/16tP9VqDg_PwPL_5xkqgqEJGij-UyKjGv?usp=sharing');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_users`
--
ALTER TABLE `tbl_users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_users`
--
ALTER TABLE `tbl_users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
